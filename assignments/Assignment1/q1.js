function getPrime(limit) {
    for (let i = limit; i < Number.MAX_SAFE_INTEGER; i++){
        let flag = false
        for (let j = i-1; j > 1; j--) 
        {
              if(i%j == 0){flag = true}    
        }
        if(flag == false){return i}
    }
}
function prime(count) {
    if(count==0) {return}
    const output = [2]
    let value = 2
    for (let index = 1; index < count; index++) {
        value = getPrime(value+1)
        output.push(value)
    }

    console.log(output)
}
prime(5)