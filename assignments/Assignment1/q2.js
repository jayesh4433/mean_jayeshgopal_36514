const books = [
    {Book_Name: 'Beginning Hybrid Mobile Application Development',ISBN_No: 9781484213155,Author: ' Mahesh Panhale',Price: 3273.00,Category: 'Web'},
    {Book_Name: 'Java-The Complete Reference',ISBN_No: 9789387432291,Author: 'Herbert Schild',Price: 934.0,Category: 'Java'},
    {Book_Name: 'Core and Advanced Java, Black Book',ISBN_No: 9789386052216,Author: ' Dreamtech Press',Price: 1135.0,Category: 'Java'},
    {Book_Name: 'Let Us C',ISBN_No: 9789387284494,Author: 'Yashavant Kanetkar',Price: 260.0,Category: 'C'},
    {Book_Name: 'C: The Complete Reference',ISBN_No: 9780070411838,Author: 'Herbert Schildt',Price: 397.0,Category: 'C'}
]

function a() {
    const affordableBook = books.filter(book=>{return book['Price'] < 500 })
    console.log(affordableBook)
}
a()
console.log('------------------------------------------------------')
console.log('------------------------------------------------------')
function b(num) {
    const UpdatePrice = books.filter(book=>{
        return book.Price > 1000
    })
    for (let index = 0; index < UpdatePrice.length; index++) {
        const book = UpdatePrice[index];
        book.Price = book.Price - ( book.Price * (num/100))
    }
    console.log(UpdatePrice)
}
b(10)
console.log('------------------------------------------------------')
console.log('------------------------------------------------------')
function print() {
    console.log(`Book Name  :   ${this.Book_Name}`)
    console.log(`ISBN Number:   ${this.ISBN_No}`)
    console.log(`Author Name:   ${this.Author}`)
    console.log(`Price      :   ${this.Price}`)
    console.log(`Category   :   ${this.Category}`)
    console.log('------------------------------------------------------')
    
}
function Book(Book_Name,ISBN_No,Author,Price,Category) {
    this.Book_Name = Book_Name
    this.ISBN_No = ISBN_No
    this.Author = Author
    this.Price = Price
    this.Category = Category
    this.print = print
}

const books2 = [
    new Book('Beginning Hybrid Mobile Application Development',9781484213155,' Mahesh Panhale',3273.00,'Web'),
    new Book('Java-The Complete Reference',9789387432291,'Herbert Schild',934.0,'Java'),
    new Book('Core and Advanced Java, Black Book',9789386052216,' Dreamtech Press',1135.0,'Java'),
    new Book('Let Us C',9789387284494,'Yashavant Kanetkar',260.0,'C'),
    new Book('C: The Complete Reference',9780070411838,'Herbert Schildt',397.0,'C')
]

books2.forEach(book => {
    book.print()
});