const express = require('express')
const db = require('../../db')
const utils = require('../../utils')
const multer = require('multer')
const upload = multer({dest:'images/'})
const fs = require('fs')

const router = express.Router()

router.get('/', (request, response) => {
  const statement = `select product.*,category.title as categoryTitle, 
  brand.title as brandTitle from product
    inner join brand on product.brandId = brand.id
    inner join category on product.categoryId = category.id`
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})

router.get('/image/:filename', (request, response) => {
  const{filename} = request.params
  const path = 'images/' + filename
  const data = fs.readFileSync(path)
  response.send(data)
})

router.post('/',upload.single('image'),(request, response) => {

  // console.log(`filename: ${request.file.originalname}`)//originalname: samsung-galaxy-a51.jpg
  // console.log(`filename: ${request.file.filename}`)//filename: 9fc2ff8e8c2df3dcbfa9f35958889174
  // console.log(`filename: ${request.file.path}`)//path: images/9fc2ff8e8c2df3dcbfa9f35958889174
  // console.log(`filename: ${request.file.size}`)//size: 54588 (it is in bytes)
  // console.log(`filename: ${request.file.destination}`)//filename: images/

  const {title, description, category, brand, price} = request.body
  const statement = `insert into product (title, description, categoryId, brandId, price, imageFile) `+
   `values ('${title}', '${description}', '${category}', '${brand}', '${price}','${request.file.filename}')`
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})

router.put('/:id',upload.single('image'),(request, response) => {
  const {id} = request.params
  const {title, description, category, brand, price} = request.body
  const statement = `update product 
    set title = '${title}', 
        description = '${description}',
        categoryId = '${category}',
        brandId = '${brand}',
        price = '${price}'
    where id = ${id}`
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})

router.delete('/:id', (request, response) => {
  const {id} = request.params
  const statement = `delete from product where id = ${id}`
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})


module.exports = router