const { request, response } = require('express')
const express = require('express')
const db = require('../../db')
const utils = require('../../utils')

const router = express.Router()

router.get('/:id',(request,response)=>{
    const {id} = request.params

    const statement = `select productReviews.*,user.firstName,user.lastName 
                    from productReviews inner join user on productReviews.userId = user.id
                    where productId = ${id}`

    db.query(statement,(error,data)=>{
        response.send(utils.createResult(error,data))
    })
})

router.get('/user/:id',(request,response)=>{
    const {id} = request.params

    const statement = `select firstName, lastName from user where id=${id}`

    db.query(statement,(error,users)=>{
        response.send(utils.createResult(error,users[0]))
    })
})

router.post('/:id',(request,response)=>{
    const {id} = request.params
    const {review,rating} = request.body

    const statement = `insert into productReviews (productId, userId, rating, review) values(
        ${id},${request.userId},${rating},'${review}'
        )`
    console.log(statement)
    db.query(statement,(error,data)=>{
        response.send(utils.createResult(error,data))
    })
})

module.exports = router