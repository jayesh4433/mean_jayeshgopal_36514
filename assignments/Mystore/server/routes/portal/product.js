const express = require('express')
const db = require('../../db')
const utils = require('../../utils')
const fs = require('fs')

const router = express.Router()

router.get('/details/:id', (request, response) => {
  const {id} = request.params
  const statement = `
  select product.*,category.title as categoryTitle,brand.title as brandTitle from product
    inner join brand on product.brandId = brand.id
    inner join category on product.categoryId = category.id
    where product.id = ${id}`
  db.query(statement, (error, products) => {
    response.send(utils.createResult(error, products[0]))
  })
})

router.get('/', (request, response) => {
  const statement = `
  select product.*,category.title as categoryTitle,brand.title as brandTitle from product
    inner join brand on product.brandId = brand.id
    inner join category on product.categoryId = category.id`
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})

router.get('/image/:filename', (request, response) => {
  const{filename} = request.params
  const path = 'images/' + filename
  const data = fs.readFileSync(path)
  response.send(data)
})

router.get('/search/:text',(request,response)=>{
  const{text} = request.params
  
  const statement = `
  select product.*,category.title as categoryTitle,brand.title as brandTitle from product
    inner join brand on product.brandId = brand.id
    inner join category on product.categoryId = category.id
    where title like '%${text}%' or description like '%${text}%'`
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})

router.post('/filter',(request,response)=>{
  const{categoryId,brandId} = request.body
  
  let categoryClause = ''
  let brandClause = ''
  //categoryId = 0 : all categories
  //brandId = 0 : all brands 

  if(categoryId != 0){ categoryClause = `categoryId = ${categoryId}`}
  if(brandId != 0){ brandClause = `brandId = ${brandId}`}

  let whereClause = ''

  if((categoryClause.length > 0)||(brandClause.length > 0)){
    whereClause = categoryClause

    if(brandClause.length > 0){
      //if categoryClause is there and add 'and' in between 
      if(whereClause.length > 0){ whereClause += ' and '}
      whereClause += brandClause
    }
    
    whereClause = ' where ' + whereClause
  }

  const statement = `
  select product.*,category.title as categoryTitle,brand.title as brandTitle from product
    inner join brand on product.brandId = brand.id
    inner join category on product.categoryId = category.id
    ${whereClause}`

    // console.log(statement)

  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})//if categoryClause is there and add 'and' in between 


module.exports = router