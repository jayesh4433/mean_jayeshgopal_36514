const express = require('express')
const db = require('../../db')
const mysql2 = require('mysql2/promise')
const utils = require('../../utils')
const { request, response } = require('express')

const router = express.Router()
const pool = mysql2.createPool({
    host: 'localhost',
    user: 'mean',
    password: 'mean',
    database: 'Mystore',
    waitForConnections: true,
    connectionLimit: 10,
    queueLimit: 0
});

router.get('/',(request,response)=>{
    const statement = `select * from userOrder where userId = ${request.userId}`
    db.query(statement,(error,data)=>{
        response.send(utils.createResult(error,data))
    })
})

router.delete('/cancel/:id',(request,response)=>{
    const {id} = request.params
    const statement = `update userOrder set orderState = 6 where id =${id}`
    db.query(statement,(error,data)=>{
        response.send(utils.createResult(error,data))
    })
})

router.post('/',(request,response)=>{


    //this will execute immediately
    (async()=>{
        const{addressId} = request.body
        //sync behaviour

        //step1: get the cart items
        let totalPrice = 0
        const [items] = await pool.query(`select * from cart where userId = ${request.userId}`)
        items.forEach(item=>{
            totalPrice += (item.price * item.quantity)
        })

        //step2: enter order main details in userOrder order
        const statement = `insert into userOrder(userId,totalAmount,addressId) 
        values(${request.userId},${totalPrice},${addressId})`

        const [result] = await pool.query(statement)

        //the insertId of newly inserted order main detils will be
        //used as orderId in the userOrderDetails Table
        const orderId = result.insertId
        //step3: insert the order product details in  userOrderDetails table
        for (let index = 0; index < items.length; index++) {
            const item = items[index];

            const insertOrderDetailsStatement =  `
            insert into userOrderDetails (orderId,productId,price,quantity,totalAmount) 
            values(${orderId},${item.productId},${item.price},${item.quantity},${(item.price*item.quantity)})
            `
            await pool.query(insertOrderDetailsStatement)
        }

        
        //step4: remove the cart items
        await pool.query(`delete from cart where userId = ${request.userId}`)

        response.send({status:'success'})
    }) ()

})

module.exports = router