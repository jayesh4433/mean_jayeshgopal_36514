import { ProductEditComponent } from './../product-edit/product-edit.component';
import { ProductAddComponent } from './../product-add/product-add.component';
import { ProductService } from './../product.service';
import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {

  products = []
  constructor(private productService: ProductService,
              private toaster:ToastrService,
              private modelService: NgbModal) { }

  ngOnInit(): void {
    this.loadProduct()
  }

  loadProduct(){
    this.productService.getProduct()
    .subscribe(response=>{
      if(response['status'] == 'success'){
        this.products = response['data']
        //console.log(this.products)
      }
      else{
        this.toaster.error(response['error'])
      }
    })
  }

  onAdd(){
    const modalRef = this.modelService.open(ProductAddComponent, {size:'lg'})
    modalRef.result.finally(()=>{
      this.loadProduct()
    })
  }
  onEdit(product){
    const modalRef = this.modelService.open(ProductEditComponent)
    //get the edit component reference
    const component = modalRef.componentInstance as ProductEditComponent
    //pre-fill data
    component.id = product.id
    component.title = product.title
    component.description = product.description
    component.price = product.price
    component.brand = product.brandId
    component.category = product.categoryId

    modalRef.result.finally(()=>{
      this.loadProduct()
    })
  }
  onDelete(product){
    this.productService.deleteProduct(product.id)
    .subscribe(response=>{
      if(response['status']=='success')
      {
        this.toaster.success('product deleted')
        this.loadProduct()
      }
      else{
        this.toaster.error(response['error'])
      }
    })
  }

}
