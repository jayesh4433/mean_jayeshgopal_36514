import { ProductService } from './../product.service';
import { CategoryService } from './../../category/category.service';
import { BrandService } from './../../brand/brand.service';
import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-product-add',
  templateUrl: './product-add.component.html',
  styleUrls: ['./product-add.component.css']
})
export class ProductAddComponent implements OnInit {

  title = ''
  price = 0
  category = 0
  brand = 0
  description = ''
  image = undefined
  //load brand and categories
  brands = []
  categories = []

  constructor(private brandService: BrandService,
              private categoryServie: CategoryService,
              private toastr: ToastrService,
              private modal: NgbActiveModal,
              private service: ProductService) { }

  ngOnInit(): void {
    this.loadBrands()
    this.loadCategories()

  }

  //load brands
  loadBrands(){
    this.brandService.getBrand()
    .subscribe(response=>{
      if(response['status'] == 'success'){
        this.brands = response['data']
       // console.log(this.brands)
      }
      else{
        this.toastr.error(response['error'])
      }
    })
  }
  //load categories
  loadCategories(){
    this.categoryServie.getCategories()
    .subscribe(response=>{
      if(response['status'] == 'success'){
        this.categories = response['data']
      }
      else{
        this.toastr.error(response['error'])
      }
    })
  }

  onImageSelect(event){
    //To get selected file
    // console.log(event.target.files[0])
    this.image = event.target.files[0]
  }

  onAdd(){
    if(this.title.length == 0 ) { this.toastr.warning('please add title') }
    else if(this.description.length == 0) { this.toastr.warning('please provide description ') }
    else if(this.price == 0) { this.toastr.warning('price can not be zero') }
    else if(this.category == 0) { this.toastr.warning('please provide category details') }
    else if(this.brand == 0) { this.toastr.warning('please provide brand details') }
    else if (this.image == undefined) { this.toastr.warning('please select an image') }
    else{
      this.service.addProduct(this.title,this.price,this.category,this.brand,this.description,this.image)
      .subscribe(response=>{
        if(response['status']=='success'){
          this.modal.dismiss('ok')
        }
        else{

        }
      })
    }
  }
  onCancel(){
    this.modal.dismiss('cancel')
  }
}
