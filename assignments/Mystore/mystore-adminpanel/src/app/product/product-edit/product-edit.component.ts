import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { BrandService } from 'src/app/brand/brand.service';
import { CategoryService } from 'src/app/category/category.service';
import { ProductService } from '../product.service';

@Component({
  selector: 'app-product-edit',
  templateUrl: './product-edit.component.html',
  styleUrls: ['./product-edit.component.css']
})
export class ProductEditComponent implements OnInit {

  id = 0
  title = ''
  price = 0
  category = 0
  brand = 0
  description = ''
  image = undefined

  brands = []
  categories = []
  constructor(private brandService: BrandService,
              private categoryServie: CategoryService,
              private toastr: ToastrService,
              private modal: NgbActiveModal,
              private service: ProductService) { }

  ngOnInit(): void {
    this.loadCategories()
    this.loadBrands()
  }

  //load brands
  loadBrands(){
    this.brandService.getBrand()
    .subscribe(response=>{
      if(response['status'] == 'success'){
        this.brands = response['data']
       // console.log(this.brands)
      }
      else{
        this.toastr.error(response['error'])
      }
    })
  }
  //load categories
  loadCategories(){
    this.categoryServie.getCategories()
    .subscribe(response=>{
      if(response['status'] == 'success'){
        this.categories = response['data']
      }
      else{
        this.toastr.error(response['error'])
      }
    })
  }

  onImageSelect(event){
    //To get selected file
    // console.log(event.target.files[0])
    this.image = event.target.files[0]
  }

  onUpdate(){
    if(this.title.length == 0 ) { this.toastr.warning('please add title') }
    else if(this.description.length == 0) { this.toastr.warning('please provide description ') }
    else if(this.price == 0) { this.toastr.warning('price can not be zero') }
    else if(this.category == 0) { this.toastr.warning('please provide category details') }
    else if(this.brand == 0) { this.toastr.warning('please provide brand details') }
    else{
      this.service.editProduct(this.id,this.title,this.price,this.category,this.brand,this.description,this.image)
      .subscribe(response=>{
        if(response['status']=='success')
        {
          this.modal.dismiss('ok')
        }
        else{
          this.toastr.error(response['error'])
        }
      })
    }
  }
  onCancel(){
    this.modal.dismiss('cancel')
  }

}
