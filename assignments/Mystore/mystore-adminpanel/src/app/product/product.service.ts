import { title } from 'process';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  private url = 'http://localhost:4433/product'
  constructor(private http:HttpClient) { }

  getProduct(){
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    }
    return this.http.get(this.url,httpOptions)
  }
  addProduct(title:string,price:number,category:number,brand:number,description:string,image:any){
    const body = new FormData()
    body.append("title",title)
    body.append("price",''+price)
    body.append("category",''+category)
    body.append("brand",''+brand)
    body.append("description",description)
    body.append("image",image)

    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    }
    return this.http.post(this.url,body,httpOptions)
  }
  editProduct(id:number,title:string,price:number,category:number,brand:number,description:string,image:any){
    const body = new FormData()
    body.append("title",title)
    body.append("price",''+price)
    body.append("category",''+category)
    body.append("brand",''+brand)
    body.append("description",description)
    if(image != undefined){ body.append("image",image) }
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    }
    return this.http.put(this.url + '/' + id,body,httpOptions)
  }
  deleteProduct(id:number){
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    }
    return this.http.delete(this.url + '/' + id,httpOptions)
  }
}
