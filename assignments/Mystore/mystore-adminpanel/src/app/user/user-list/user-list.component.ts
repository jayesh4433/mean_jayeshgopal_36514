import { ToastrService } from 'ngx-toastr';
import { UserService } from './../user.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  users = []
  constructor(private service: UserService,
              private toastr: ToastrService) { }

  ngOnInit(): void {
    this.loadUser()
  }

  loadUser(){
    this.service.getUser()
    .subscribe(response=>{
      if(response['status'] == 'success'){
        this.users = response['data']
      }
    })
  }
  setStatus(user,status:number){
    this.service.chanageStatus(user.id,status)
    .subscribe(response=>{
      if(response['status'] == 'success'){
        this.loadUser()
      }
      else{
        this.toastr.error(response['error'])
      }
    })
  }

}
