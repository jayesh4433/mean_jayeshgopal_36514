import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class BrandService {

  private url = 'http://localhost:4433/brand'
  constructor(private http:HttpClient) { }

  getBrand(){
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    }
    return this.http.get(this.url,httpOptions)
  }
  addBrand(title:string,description:string){
    const body = {
      title: title,
      description: description
    }
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    }
    return this.http.post(this.url,body,httpOptions)
  }
  editBrand(id:number,title:string,description:string){
    const body = {
      title: title,
      description: description
    }
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    }
    return this.http.put(this.url + '/' + id,body,httpOptions)
  }
  deleteBrand(id:number){
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    }
    return this.http.delete(this.url + '/' + id,httpOptions)
  }
}
