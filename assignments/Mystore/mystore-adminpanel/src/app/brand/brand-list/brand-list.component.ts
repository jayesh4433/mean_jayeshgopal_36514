import { BrandEditComponent } from './../brand-edit/brand-edit.component';
import { BrandAddComponent } from './../brand-add/brand-add.component';
import { BrandService } from './../brand.service';
import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-brand-list',
  templateUrl: './brand-list.component.html',
  styleUrls: ['./brand-list.component.css']
})
export class BrandListComponent implements OnInit {

  brands = []

  constructor(private toaster:ToastrService,
              private service: BrandService,
              private modelService: NgbModal) { }

  ngOnInit(): void {
    this.loadBrands()
  }

  loadBrands(){
    this.service.getBrand()
    .subscribe(response=>{
      if(response['status'] == 'success'){
        this.brands = response['data']
       // console.log(this.brands)
      }
      else{
        this.toaster.error(response['error'])
      }
    })
  }

  onAdd(){
    const modalRef = this.modelService.open(BrandAddComponent)
    modalRef.result.finally(()=>{
      this.loadBrands()
    })
  }
  onEdit(brand){
   
    const modalRef = this.modelService.open(BrandEditComponent)

    //get the edit component reference
    const component = modalRef.componentInstance as BrandEditComponent

    //pre-fill data
    component.id = brand.id
    component.title = brand.title
    component.description = brand.description

    modalRef.result.finally(()=>{
      this.loadBrands()
    })
  }
  onDelete(brand){
    this.service.deleteBrand(brand.id)
    .subscribe(response=>{
      if(response['status']=='success')
      {
        this.toaster.success('category deleted')
        this.loadBrands()
      }
      else{
        this.toaster.error(response['error'])
      }
    })
  }

}
