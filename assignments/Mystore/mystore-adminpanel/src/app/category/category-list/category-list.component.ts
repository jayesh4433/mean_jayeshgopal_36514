import { CategoryEditComponent } from './../category-edit/category-edit.component';
import { title } from 'process';
import { CategoryAddComponent } from './../category-add/category-add.component';
import { NgbModal} from '@ng-bootstrap/ng-bootstrap';
import { CategoryService } from './../category.service';
import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-category-list',
  templateUrl: './category-list.component.html',
  styleUrls: ['./category-list.component.css']
})
export class CategoryListComponent implements OnInit {

  categories = []

  constructor(private toaster:ToastrService,
              private service: CategoryService,
              private modelService: NgbModal) { }

  ngOnInit(): void {
    this.loadCategories()
  }

  loadCategories(){
    this.service.getCategories()
    .subscribe(response=>{
      if(response['status'] == 'success'){
        this.categories = response['data']
       // console.log(this.categories)
      }
      else{
        this.toaster.error(response['error'])
      }
    })
  }
  onAdd(){
    const modalRef = this.modelService.open(CategoryAddComponent)
    modalRef.result.finally(()=>{
      this.loadCategories()
    })
  }
  onEdit(category){
   
    const modalRef = this.modelService.open(CategoryEditComponent)

    //get the edit component reference
    const component = modalRef.componentInstance as CategoryEditComponent

    //pre-fill data
    component.id = category.id
    component.title = category.title
    component.description = category.description

    modalRef.result.finally(()=>{
      this.loadCategories()
    })
  }
  onDelete(category){
    this.service.deleteCategory(category.id)
    .subscribe(response=>{
      if(response['status']=='success')
      {
        this.toaster.success('category deleted')
        this.loadCategories()
      }
      else{
        this.toaster.error(response['error'])
      }
    })
  }

}
