import { ToastrService } from 'ngx-toastr';
import { CategoryService } from './../category.service';
import { Component, OnInit } from '@angular/core';
import { title } from 'process';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-category-add',
  templateUrl: './category-add.component.html',
  styleUrls: ['./category-add.component.css']
})
export class CategoryAddComponent implements OnInit {

  title = ''
  description = ''

  constructor(private service: CategoryService,
              private modal: NgbActiveModal,
              private toastr: ToastrService) { }

  ngOnInit(): void {
  }
  onAdd(){
    if(this.title.length == 0 )
    {
      this.toastr.warning('please add title')
    }
    else if(this.description.length == 0)
    {
      this.toastr.warning('please provide description ')
    }
    else{
      this.service.addCategory(this.title,this.description)
      .subscribe(response=>{
        if(response['status']=='success'){
          this.modal.dismiss('ok')
        }
        else{

        }
      })
    }
  }
  onCancel(){
    this.modal.dismiss('cancel')
  }

}
