import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  private url = 'http://localhost:4433/order'
  constructor(private http: HttpClient) { }

  getOrders(){
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    }
    return this.http.get(this.url,httpOptions)
  }

  updateStatus(id:number,status:number){
    const body = {
      status: status
    }
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    }
    return this.http.put(this.url+'/updatestatus/'+id,body,httpOptions)
  }
}
