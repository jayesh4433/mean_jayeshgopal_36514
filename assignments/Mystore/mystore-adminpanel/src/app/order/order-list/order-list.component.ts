import { ToastrService } from 'ngx-toastr';
import { OrderService } from './../order.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-order-list',
  templateUrl: './order-list.component.html',
  styleUrls: ['./order-list.component.css']
})
export class OrderListComponent implements OnInit {

  orders = []
  constructor(private service: OrderService,
              private toastr: ToastrService) { }

  ngOnInit(): void {
    this.loadOrders();
  }
  loadOrders(){
    this.service.getOrders()
    .subscribe(response=>{
      if(response['status']=='success'){
        this.orders = response['data']
      }
      else{
        this.toastr.error(response['error'])
      }
    })
  }

  onUpdateSatus(order,status:number){
    this.service.updateStatus(order.id,status)
    .subscribe(response=>{
      if(response['status']=='success'){
        this.toastr.success('Successfully updated order state')
        this.loadOrders()
      }
      else{
        this.toastr.error(response['error'])
      }
    })
  }

}
