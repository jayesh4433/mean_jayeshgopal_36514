import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AdminService implements CanActivate{

  private url = 'http://localhost:4433/admins'
  constructor(private httpClient: HttpClient,
              private router: Router) { }
 

  signin(email:string, password:string){
    const body = {
      email: email,
      password: password
    }

    return this.httpClient.post(this.url + '/signin',body)
  }

  //used to check if route can be activated
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean{
    if(!sessionStorage['token']){
      this.router.navigate(['/signin'])
    }
    else{
      return true
    }
  }
}
