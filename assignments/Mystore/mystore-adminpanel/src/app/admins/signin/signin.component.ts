import { ToastrService } from 'ngx-toastr';
import { AdminService } from './../admin.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {

  email = ''
  password = ''

  constructor(private service: AdminService,
              private toaster: ToastrService,
              private router: Router) { }

  ngOnInit(): void {
    
  }

  signin(){
    if(this.email.length == 0){
     this.toaster.warning('Please enter email') 
    }
    else if(this.password.length ==0){
      this.toaster.warning('Please enter password')
    }
    else{
        this.service.signin(this.email,this.password)
      .subscribe(response=>{
        if(response['status'] == 'success'){
          this.toaster.success('Welcome')

          //cache the user details along with token
          const user = response['data']
          sessionStorage['user_name'] = user['firstName'] + ' ' + user['lastName']
          sessionStorage['token'] = user['token']
          this.router.navigate(['/product-list'])
        }
        else{
          this.toaster.error(`${response['error']}`)
        }
      })
    }
  }


}
