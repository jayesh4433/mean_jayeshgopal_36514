import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { AddressService } from '../address.service';

@Component({
  selector: 'app-address-edit',
  templateUrl: './address-edit.component.html',
  styleUrls: ['./address-edit.component.css']
})
export class AddressEditComponent implements OnInit {

  id = 0
  title = ''
  line1 =''
  line2 = ''
  city = ''
  state = ''
  zipCode = ''
  
  constructor(private service: AddressService,
              private modal: NgbActiveModal,
              private toastr: ToastrService) { }

  ngOnInit(): void {
  }

  onUpdate(){
    if(this.title.length == 0){this.toastr.warning('Please enter title')}
    else if(this.line1.length == 0){this.toastr.warning('Please enter address')}
    else if(this.line2.length == 0){this.toastr.warning('Please enter address')}
    else if(this.city.length == 0){this.toastr.warning('Please enter city name')}
    else if(this.state.length == 0){this.toastr.warning('Please enter state name')}
    else if(this.zipCode.length == 0){this.toastr.warning('Please enter zip code')}
    else if(this.id == 0){this.toastr.warning('Incorrect Id')}
    else{
        this.service.updateAddresses(this.id,this.title,this.line1,this.line2,
                                  this.city,this.state,this.zipCode)
      .subscribe(response=>{
        if(response['status'] == 'success'){
          this.modal.dismiss('ok')
        }
        else{
          console.log(response['error'])
          this.toastr.error(response['error'])
        }
      })
    } 
  }
  onCancel(){
    this.modal.dismiss('cancel')
  }

}
