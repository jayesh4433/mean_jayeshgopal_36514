import { ToastrService } from 'ngx-toastr';
import { AddressService } from './../address.service';
import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-address-add',
  templateUrl: './address-add.component.html',
  styleUrls: ['./address-add.component.css']
})
export class AddressAddComponent implements OnInit {

  title = ''
  line1 =''
  line2 = ''
  city = ''
  state = ''
  zipCode = ''
  
  constructor(private service: AddressService,
              private modal: NgbActiveModal,
              private toastr: ToastrService) { }

  ngOnInit(): void {
  }

  onAdd(){
    if(this.title.length == 0){this.toastr.warning('Please enter title')}
    else if(this.line1.length == 0){this.toastr.warning('Please enter address')}
    else if(this.line2.length == 0){this.toastr.warning('Please enter address')}
    else if(this.city.length == 0){this.toastr.warning('Please enter city name')}
    else if(this.state.length == 0){this.toastr.warning('Please enter state name')}
    else if(this.zipCode.length == 0){this.toastr.warning('Please enter zip code')}
    else{
        this.service.addAddresses(this.title,this.line1,this.line2,
                                  this.city,this.state,this.zipCode)
      .subscribe(response=>{
        if(response['status'] == 'success'){
          this.modal.dismiss('ok')
        }
        else{
          this.toastr.error(response['error'])
        }
      })
    } 
  }
  onCancel(){
    this.modal.dismiss('cancel')
  }

}
