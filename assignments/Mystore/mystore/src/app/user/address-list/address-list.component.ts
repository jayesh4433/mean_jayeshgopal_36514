import { AddressEditComponent } from './../address-edit/address-edit.component';
import { AddressService } from './../address.service';
import { AddressAddComponent } from './../address-add/address-add.component';
import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-address-list',
  templateUrl: './address-list.component.html',
  styleUrls: ['./address-list.component.css']
})
export class AddressListComponent implements OnInit {

  addresses = []
  constructor(private modal: NgbModal,
              private service: AddressService,
              private toastr: ToastrService) { }

  ngOnInit(): void {
    this.loadAddress()
  }

  loadAddress(){
    this.service.getAddresses()
    .subscribe(response=>{
      if(response['status'] == 'success'){
        this.addresses = response['data']
      }
      else{
        this.toastr.error(response['error'])
      }
    })
  }

  onAdd(){
    const modalRef = this.modal.open(AddressAddComponent,{size:'lg'})
    modalRef.result.finally(()=>{
      this.loadAddress()
    })
  }
  onEdit(address){
    const modalRef = this.modal.open(AddressEditComponent,{size:'lg'})
    
    const instance = modalRef.componentInstance as AddressEditComponent

    instance.id = address.id
    instance.title = address.title
    instance.zipCode = address.zipCode
    instance.line1 = address.line1
    instance.line2 = address.line2
    instance.city = address.city
    instance.state = address.state

    modalRef.result.finally(()=>{
      this.loadAddress()
    })
  }

  onDelete(address){
    this.service.deleteAddresses(address.id)
    .subscribe(response=>{
      if(response['status']=='success')
      {
        this.toastr.success('address deleted')
        this.loadAddress()
      }
      else{
        this.toastr.error(response['error'])
      }
    })
  }

}
