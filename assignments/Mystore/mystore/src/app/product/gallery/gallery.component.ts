import { Router } from '@angular/router';
import { CartService } from './../cart.service';
import { ProductService } from './../product.service';
import { CategoryService } from './../category.service';
import { BrandService } from './../brand.service';
import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.css']
})
export class GalleryComponent implements OnInit {

  categoryId = 0
  brandId = 0
  brands = []
  categories = []
  products = []

  constructor(private brandService:BrandService,
              private categoryServie: CategoryService,
              private productService: ProductService,
              private cartService: CartService,
              private toastr: ToastrService,
              private router: Router) { }

  ngOnInit(): void {
    this.loadBrands()
    this.loadCategories()
    this.filterProduct()
  }

  filterProduct(){
    this.productService.filterProduct(this.categoryId,this.brandId)
    .subscribe(response=>{
      if(response['status'] == 'success'){
        this.products = response['data']
        //console.log(this.products)
      }
      else{
        this.toastr.error(response['error'])
      }
    })
  }

  //load brands
  loadBrands(){
    this.brandService.getBrand()
    .subscribe(response=>{
      if(response['status'] == 'success'){
        this.brands = response['data']
       // console.log(this.brands)
      }
      else{
        this.toastr.error(response['error'])
      }
    })
  }
  //load categories
  loadCategories(){
    this.categoryServie.getCategories()
    .subscribe(response=>{
      if(response['status'] == 'success'){
        this.categories = response['data']
      }
      else{
        this.toastr.error(response['error'])
      }
    })
  }

  onAddToCart(product){
    this.cartService
    .addItemToCart(product.id,product.price,1)
    .subscribe(response=>{
      if(response['status'] == 'success'){
        this.toastr.success(`${product.title} added to cart`)
      }
      else{
        this.toastr.error(response['error'])
      }
    })
  }

  showProductDetails(product){
    this.router.navigate(['/home/product/product-info'],{queryParams:{ id: product.id }})
  }

}
