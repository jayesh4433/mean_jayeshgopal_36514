import { SelectAddressComponent } from './../../order/select-address/select-address.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CartService } from './../cart.service';
import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {

  products = []
  totalPrice = 0
  constructor(private service: CartService,
              private toastr: ToastrService,
              private modal: NgbModal) { }

  ngOnInit(): void {
    this.loadCartItems()
  }

  loadCartItems(){
    this.service
    .getCartItems()
    .subscribe(response=>{
      if(response['status'] == 'success'){
        this.totalPrice = 0
        this.products = response['data']
        this.products.forEach(product=>{
          this.totalPrice += (product.price * product.quantity)
        })
      }
      else{
        this.toastr.error(response['error'])
      }
    })
  }

  reduceQuantity(product){
    const quantity = product.quantity - 1
    this.totalPrice -= product.price
    this.updateQuantity(product.id,quantity)
  }
  increaseQuantity(product){
    const quantity = product.quantity + 1
    this.totalPrice -= product.price
    this.updateQuantity(product.id,quantity)
  }

  updateQuantity(id,quantity){
    if(quantity == 0){
      this.service
      .deleteItemFromCart(id)
      .subscribe(response=>{
        if(response['status'] == 'success'){
          this.loadCartItems()
        }
        else{
          this.toastr.error(response['error'])
        }
      })
    }else{
      this.service
      .updateItemToCart(id,quantity)
      .subscribe(response=>{
        if(response['status'] == 'success'){
          this.loadCartItems()
        }
        else{
          this.toastr.error(response['error'])
        }
      })
    }
  }

  onPlaceOrder(){
     const modalRef = this.modal.open(SelectAddressComponent,{size:'lg'})
     modalRef.result.finally(()=>{
       this.loadCartItems()
     })
  }
}
