import { ToastrService } from 'ngx-toastr';
import { ProductService } from './../product.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProductReviewService } from '../product-review.service';

@Component({
  selector: 'app-product-info',
  templateUrl: './product-info.component.html',
  styleUrls: ['./product-info.component.css']
})
export class ProductInfoComponent implements OnInit {

  review = ''
  rating = 0
  reviews = []
  product = undefined
  private id:number
  constructor(private activatedRoute: ActivatedRoute,
              private service: ProductService,
              private toastr: ToastrService,
              private productReview: ProductReviewService) { }

  ngOnInit(): void {
    this.id = this.activatedRoute.snapshot.queryParams['id']
    // console.log(`id = ${this.id}`)
   this.loadProduct();
   this.loadReview();
  }
  loadProduct(){
    this.service.getProductInfo(this.id)
    .subscribe(response=>{
      if(response['status']=='success'){
        this.product = response['data']
        // console.log(this.product)
      }
      else{
        this.toastr.error(response['error'])
      }
    })
  }
  loadReview(){
    this.productReview.getReview(this.id)
    .subscribe(response=>{
      if(response['status']=='success'){
        this.reviews = response['data']
        console.log(this.reviews)
      }
      else{
        this.toastr.error(response['error'])
      }
    })
  }
  onSubmitReview(){
    this.productReview.submitReview(this.review,this.rating,this.product.id)
    .subscribe(response=>{
      if(response['status']=='success'){
        this.toastr.success('Review Submitted')
      }
      else{
        this.toastr.error(response['error'])
      }
    })
  }

}
