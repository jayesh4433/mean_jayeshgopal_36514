import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { utils } from 'protractor';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  private url = 'http://localhost:4444/product'

  constructor(private http:HttpClient) { }

  getProductInfo(id:number){
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    }
    return this.http.get(this.url+"/details/"+id,httpOptions)
  }

  filterProduct(categoryId:number,brandId:number){
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    }
    const body = {
      categoryId: categoryId,
      brandId: brandId
    }
    return this.http.post(this.url+'/'+'filter',body,httpOptions)
  }

}
