import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ProductReviewService {

  private url = 'http://localhost:4444/product-review'

  constructor(private http:HttpClient) { }

  getReview(id:number){
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    }
    return this.http.get(this.url+'/'+id,httpOptions)
  }
  submitReview(review:String,rating:number,id:number){
    const body = {
      review: review,
      rating: rating
    }
  const httpOptions = {
    headers: new HttpHeaders({
      token: sessionStorage['token']
     })
    }
    return this.http.post(this.url+'/'+id,body,httpOptions)
  }
}
