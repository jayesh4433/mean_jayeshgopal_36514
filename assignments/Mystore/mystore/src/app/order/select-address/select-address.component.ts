import { OrderService } from './../order.service';
import { Component, OnInit } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { AddressAddComponent } from 'src/app/user/address-add/address-add.component';
import { AddressService } from 'src/app/user/address.service';

@Component({
  selector: 'app-select-address',
  templateUrl: './select-address.component.html',
  styleUrls: ['./select-address.component.css']
})
export class SelectAddressComponent implements OnInit {

  addresses = []
  constructor(private activemodal: NgbActiveModal,
              private modal: NgbModal,
              private addressService: AddressService,
              private orderService: OrderService,
              private toastr: ToastrService) { }

  ngOnInit(): void {
    this.loadAddress()
  }

  loadAddress(){
    this.addressService.getAddresses()
    .subscribe(response=>{
      if(response['status'] == 'success'){
        this.addresses = response['data']
      }
      else{
        this.toastr.error(response['error'])
      }
    })
  }

  onAdd(){
    const modalRef = this.modal.open(AddressAddComponent,{size:'lg'})
    modalRef.result.finally(()=>{
      this.loadAddress()
    })
  }
  onSelect(address){
    this.orderService.placeOrder(address.id)
    .subscribe(response=>{
      if(response['status'] == 'success'){
        this.toastr.success('Congratulations!!! Placed your order')
        this.activemodal.dismiss('ok')
      }
      else{
        this.toastr.error(response['error'])
      }
    })
  }

  onCancel(){
    this.activemodal.dismiss('cancel')
  }

}
