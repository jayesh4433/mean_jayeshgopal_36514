import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  private url = 'http://localhost:4444/order'
  constructor(private http: HttpClient) { }

  placeOrder(addressId:number){
    const body = {
      addressId: addressId
    }
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    }
    return this.http.post(this.url,body,httpOptions)
  }

  cancelOrder(id:number){
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    }
    return this.http.delete(this.url+'/cancel/'+id,httpOptions)
  }

  getOrders(){
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    }
    return this.http.get(this.url,httpOptions)
  }
}
