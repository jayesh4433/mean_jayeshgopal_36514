import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  firstName = ''
  lastName = ''
  email = ''
  password = ''
  confirmpassword = ''
  phone = ''
  constructor(private toastr:ToastrService,
              private service: AuthService,
              private router: Router) { }

  ngOnInit(): void {
  }
  onSignup(){
    if(this.firstName.length == 0){ this.toastr.warning('Please enter first name')}
    else if(this.lastName.length == 0){ this.toastr.warning('Please enter last name')}
    else if(this.email.length == 0){ this.toastr.warning('Please enter email')}
    else if(this.phone.length == 0){ this.toastr.warning('Please enter phone number')}
    else if(this.password.length == 0){ this.toastr.warning('Please enter password')}
    else if(this.confirmpassword.length == 0){ this.toastr.warning('Please confirm password')}
    else if(this.password != this.confirmpassword){ this.toastr.warning('password does not match')}
    else{
      this.service.
      signup(this.firstName,this.lastName,this.email,this.phone,this.password)
      .subscribe(response=>{
        if(response['status'] == 'success'){
          this.toastr.success('you have registerd successfully')
          this.router.navigate(['/auth/signin'])
        }
        else{
          this.toastr.error(response['error'])
        }
      })
    }
  }

}
