import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {

  email = ''
  password = ''
  constructor(private toastr:ToastrService,
              private service: AuthService,
              private router: Router) { }

  ngOnInit(): void {
  }
  onSignin(){
    if(this.email.length == 0){ this.toastr.warning('Please enter email')}
    else if(this.password.length == 0){ this.toastr.warning('Please enter password')}
    else{
      this.service.
      signin(this.email,this.password)
      .subscribe(response=>{
        if(response['status'] == 'success'){
          // console.log(response)
          this.toastr.success('Welcome')

          //cache the user details along with token
          const user = response['data']
          sessionStorage['user_name'] = user['firstName'] + ' ' + user['lastName']
          sessionStorage['token'] = user['token']
          this.router.navigate(['/home/'])
        }
        else{
          this.toastr.error(response['error'])
        }
      })
    }
  }
}
