import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService implements CanActivate{

  private url = 'http://localhost:4444/user'
  constructor(private http: HttpClient,
              private router: Router) { }
  

  signin(email:string, password:string){
    const body = {
      email: email,
      password: password
    }
    return this.http.post(this.url + '/signin',body)
  }
  signup(firstName:string,lastName:string,email:string,phone:string,password:string){
    const body = {
      firstName: firstName,
      lastName: lastName,
      email: email,
      phone: phone,
      password: password
    }
    return this.http.post(this.url + '/signup',body)
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean{
    if(!sessionStorage['token']){
      this.router.navigate(['/auth/signin'])
    }
    else{
      return true
    }
  }
}
