
CREATE TABLE Pizza_User (
id INT(11) AUTO_INCREMENT PRIMARY KEY,
name VARCHAR(25),
password VARCHAR(200),
mobile VARCHAR(10),
address VARCHAR(70),
email VARCHAR(40),
role  VARCHAR(10),  
CONSTRAINT EMAIL_UNIQUE UNIQUE (email)); 


CREATE TABLE PIZZA_ITEMS (
id INT(11) AUTO_INCREMENT PRIMARY KEY,
name VARCHAR(40),
type VARCHAR(20),
category VARCHAR(30),
description VARCHAR(120),
price float,
image VARCHAR(120));



