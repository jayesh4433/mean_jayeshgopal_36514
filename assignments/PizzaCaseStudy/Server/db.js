const mysql = require('mysql2')

const pool = mysql.createPool({
    host: 'localhost',
    user: 'mean',
    password: 'mean',
    database: 'online_pizza_db',
    port: 3306,
    waitForConnections: true,
    connectionLimit: 10,
    queueLimit: 0
  })

  module.exports = {
      connection : pool
  }