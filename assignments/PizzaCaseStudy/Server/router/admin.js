const express = require('express')
const db = require('../db')
const utils = require('../util')

const router = express.Router()

//create
router.post('/',(request,response)=>{
    const {name,type,category,description,price,image} = request.body

    const statement = 'INSERT INTO PIZZA_ITEMS (name,type,category,description,price,image) '+
    `VALUES('${name}','${type}','${category}','${description}',${price},'${image}')`

    db.connection.query(statement,(error,data)=>{
        response.send(utils.createResult(error,data))
    })
})

//read
router.get('/',(request,response)=>{

    const statement = 'SELECT id,name,type,category,description,price,image FROM PIZZA_ITEMS'

    db.connection.query(statement,(error,data)=>{
        response.send(utils.createResult(error,data))
    })
})

//update
router.put('/:id',(request,response)=>{

    const {name,type,category,description,price,image} = request.body

    const statement = `UPDATE PIZZA_ITEMS SET name='${name}',type='${type}',
    category='${category}',description='${description}',price=${price},image='${image}' WHERE id=${request.params.id}`

    db.connection.query(statement,(error,data)=>{
        response.send(utils.createResult(error,data))
    })
})

//delete
router.delete('/:id',(request,response)=>{

    const statement = `DELETE FROM PIZZA_ITEMS WHERE id=${request.params.id}`

    db.connection.query(statement,(error,data)=>{
        response.send(utils.createResult(error,data))
    })
})

module.exports = router