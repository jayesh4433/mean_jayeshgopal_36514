const express = require('express')
const db = require('../db')
const util = require('../util')
const crypto = require('crypto-js')

const router = express.Router()

router.post('/signup',(request,response)=>{
    const {name,mobile,address,email,password} = request.body
    const role = 'customer'
    const encryptPassword = crypto.SHA512(password)
    const statement = 'INSERT INTO Pizza_User (name,mobile,address,role,email,password) '+
    `VALUES ('${name}','${mobile}','${address}','${role}','${email}','${encryptPassword}')`

    db.connection.query(statement,(error,user)=>{
        response.send(util.createResult(error,user))
    })
})

router.post('/login',(request,response)=>{
    const {email,password} = request.body
    const encryptPassword = crypto.SHA512(password)
    const statement = 'select name,mobile,address,role from Pizza_User '+
    `where email = '${email}' and password = '${encryptPassword}'`

    db.connection.query(statement,(error,users)=>{
        const result = {}
        if(error)
        {
            result['status'] = 'error'
            result['data'] = error
        }
        else
        {
            if(users.length == 0)
            {
                result['status'] = 'error'
                result['data'] = 'Incorrect User Name / Password'
            }
            else
            {
                result['status'] = 'success'
                result['data'] = users[0]
            }
        }
        response.send(result)
    })
})

module.exports = router