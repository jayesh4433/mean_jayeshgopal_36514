const express = require('express')
const db = require('../db')
const utils = require('../util')
const { request, response } = require('express')

const router = express.Router()

router.get('/:type',(request,response)=>{
    const type = request.params.type

    const statement = 'SELECT id,name,type,category,description,price,image FROM PIZZA_ITEMS '+
    `ORDER BY ${type}`

    db.connection.query(statement,(error,data)=>{
        response.send(utils.createResult(error,data))
    })
})
router.get('/',(request,response)=>{

    const statement = 'SELECT name,type,category,description,price,image FROM PIZZA_ITEMS '

    db.connection.query(statement,(error,data)=>{
        response.send(utils.createResult(error,data))
    })
})

module.exports = router