const express = require('express')
const bodyParser = require('body-parser')
const routeruser = require('./router/user')
const routerAdmin = require('./router/admin')
const routerCustomer = require('./router/customer')

const app = express()


app.use(bodyParser.json())
app.use('/user',routeruser)
app.use('/admin',routerAdmin)
app.use('/customer/',routerCustomer)


app.listen(4433,'localhost',()=>{
    console.log('Server started on port 4433')
})
