const crypto = require('crypto-js')

const adminPassword = 'admin'
const akashPassword = 'akash'
const shubhamPassword = 'shubham'
const rahulPassword = 'rahul'

console.log(`SHA512 adminPassword    = ${crypto.SHA512(adminPassword)}`)
console.log(`SHA512 akashPassword    = ${crypto.SHA512(akashPassword)}`)
console.log(`SHA512 shubhamPassword    = ${crypto.SHA512(shubhamPassword)}`)
console.log(`SHA512 rahulPassword    = ${crypto.SHA512(rahulPassword)}`)