const mysql = require('mysql2')

/* function openConnection(){
    const connection = mysql.createConnection({
        host:'localhost',
        user:'root',
        password:'manager',
        database:'mystore'
    })
    return connection
} */

const pool = mysql.createPool({
    host: 'localhost',
    user: 'mean',
    password: 'mean',
    database: 'mystore',
    port: 3306,
    waitForConnections: true,
    connectionLimit: 10,
    queueLimit: 0
  })

module.exports = {
    connection : pool
}