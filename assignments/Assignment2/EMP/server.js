const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const routerEmp = require('./routes/Emp')

app.use(bodyParser.json())
app.use('/Emp',routerEmp)

app.listen(4433,'localhost',()=>{
    console.log('Server is started on Port 4433')
})