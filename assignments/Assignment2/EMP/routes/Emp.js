const express = require('express')
/* const mysql = require('mysql') */
/* const dbObj = require('../db') */
const db = require('../db')
const crypto = require('crypto-js')
const utils = require('../utils')
const router = express.Router()
 
//Login
router.post('/login',(request,response)=>{
    const {email,password} = request.body
    const encryptedPassword = crypto.SHA256(password)
    const statement = `select Emp_ID,Name,Mobile,Address,Salary,Designation from EMP where Email='${email}' and Password='${encryptedPassword}'`
     
    db.connection.query(statement,(error,emps)=>{
        const result = {}
        if(error)
        {
            result['status'] = 'error'
            result['error'] = error
        }
        else
        {
            if(emps.length == 0)
            {
                result['status'] = 'error'
                result['error'] = 'Invalid credentials'
            }
            else
            {
                result['status'] = 'success'
                result['data'] = emps[0]
            }
        }
        response.send(result)
    })
})
//create
router.post('/',(request,response)=>{
    const body = request.body
    const encryptedPassword = crypto.SHA256(body.Password)
    const statement = `INSERT INTO EMP (Name,Email,Password,Mobile,Address,Salary,Designation)`+
     `VALUES('${body.Name}','${body.Email}','${encryptedPassword}',${body.Mobile},'${body.Address}',${body.Salary},'${body.Designation}')`
     
    db.connection.query(statement,(error,data)=>{
        response.send(utils.createResult(error,data))
    })
})
//read
router.get('/',(request,response)=>{
    
    const statement = 'select Emp_ID,Name,Email,Mobile,Address,Salary,Designation from EMP'

    db.connection.query(statement,(error,data)=>{
        response.send(utils.createResult(error,data))
    })
})
//update
router.put('/:id',(request,response)=>{
    const id = request.params.id
    const body = request.body
    const encryptedPassword = crypto.SHA256(body.Password)
    const statement = `UPDATE EMP SET Name='${body.Name}',Email='${body.Email}',Password='${encryptedPassword}',`+
    `Mobile=${body.Mobile},Address='${body.Address}',Salary=${body.Salary},Designation='${body.Designation}' WHERE Emp_ID=${id}`

    db.connection.query(statement,(error,data)=>{
        response.send(utils.createResult(error,data))
    })
})
//delete
router.delete('/:id',(request,response)=>{
    
    const statement = `DELETE FROM EMP WHERE Emp_ID=${request.params.id}`

    db.connection.query(statement,(error,data)=>{
        response.send(utils.createResult(error,data))
    })
})

module.exports = router