const mysql = require('mysql')

function openConnection(){
    const connection = mysql.createConnection({
        host:'localhost',
        user:'root',
        password:'manager',
        database:'mystore'
    })
    return connection
}

module.exports = {
    connection : openConnection
}