const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const routerEmp = require('./routes/Dept')

app.use(bodyParser.json())
app.use(routerEmp)

app.listen(4433,'localhost',()=>{
    console.log('Server is started on Port 4433')
})