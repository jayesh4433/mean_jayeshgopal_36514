const express = require('express')
const mysql = require('mysql')
const dbObj = require('../db')

const router = express.Router()
 
//create
router.post('/Dept',(request,response)=>{
    const connection = dbObj.connection()
    const body = request.body
    const statement = `INSERT INTO DEPT (Name,Location) VALUES('${body.Name}','${body.Location}')`
     
    connection.query(statement,(error,result)=>{
        if(error){
            console.error(`error: ${error}`)
        }
        connection.end()
        response.send(result)
    })
})
//read
router.get('/Dept',(request,response)=>{
    const connection = dbObj.connection()
    
    const statement = 'select Dept_ID,Name,Location from DEPT'

    connection.query(statement,(error,result)=>{
        if(error){
            console.error(`error: ${error}`)
        }
        connection.end()
        response.send(result)
        
    })
})
//update
router.put('/Dept/:id',(request,response)=>{
    const connection = dbObj.connection()
    const id = request.params.id
    const body = request.body

    const statement = `UPDATE DEPT SET Name='${body.Name}',Location='${body.Location}'`

    connection.query(statement,(error,result)=>{
        if(error){
            console.error(`error: ${error}`)
        }
        connection.end()
        response.send(result)
        
    })
})
//delete
router.delete('/Dept/:id',(request,response)=>{
    const connection = dbObj.connection()
    
    const statement = `DELETE FROM DEPT WHERE Dept_ID=${request.params.id}`

    connection.query(statement,(error,result)=>{
        if(error){
            console.error(`error: ${error}`)
        }
        connection.end()
        response.send(result)
    })
})

module.exports = router