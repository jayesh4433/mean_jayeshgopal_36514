const express = require('express')
const mysql = require('mysql')

const router = express.Router()

router.get('/product', (request, response) => {
  const connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'manager',
    database: 'mystore',
    port: '3306'
})

  const statement = 'select id, title, description, price from product'

  connection.query(statement, (error, result) => {     
    if (error) {
      console.error(`error: ${error}`)
    } 
    else {
      console.log(result)
    }
    connection.end()

    response.send(result)
  })
})

router.post('/product', (request, response) => {

    const title = request.body.title
    const description = request.body.description
    const price = request.body.price
  
    const connection = mysql.createConnection({
        host: 'localhost',
        user: 'root',
        password: 'manager',
        database: 'mystore',
        port: '3306'
    })
  
    const statement = `insert into product (title, description, price) values ('${title}', '${description}', '${price}')`
  
    connection.query(statement, (error, result) => {
      if (error) {
        console.error(`error: ${error}`)
      }
      else {
        console.log(result)
      }
      connection.end()
  
      response.send(result)
    })
    
})

router.put('/product/:id', (request, response) => {
    const id = request.params.id
    const price = request.body.price
    const title = request.body.title
    const description = request.body.description
  
    const connection = mysql.createConnection({
        host: 'localhost',
        user: 'root',
        password: 'manager',
        database: 'mystore',
        port: '3306'
    })
  
    const statement = `update product set title = '${title}', description = '${description}', price = ${price} where id = ${id}`
  
    connection.query(statement, (error, result) => {
      if (error) {
        console.error(`error: ${error}`)
      }
      else {
        console.log(result)
      }
      connection.end()

      response.send(result)
    })
  
  })

  router.delete('/product/:id', (request, response) => {
    const id = request.params.id
  
    const connection = mysql.createConnection({
        host: 'localhost',
        user: 'root',
        password: 'manager',
        database: 'mystore',
        port: '3306'
    })

    const statement = `delete from product where id = ${id}`
  
    connection.query(statement, (error, result) => {
      if (error) {
        console.error(`error: ${error}`)
      }
      else {
      console.log(result)
    }
      connection.end()
      
      response.send(result)
    })
    
  })

module.exports = router