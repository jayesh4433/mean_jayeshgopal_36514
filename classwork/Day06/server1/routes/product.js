const express = require('express')
const { request } = require('http')
const { response } = require('express')

const router = express.Router()

const products = [
    {id: 1, title: 'product1', description: 'description of product 1', price: 100}
]
//read
router.get('/product',(request,response)=>{
    /* const strProduct = JSON.stringify(products)
    response.setHeader('Content-Type','application/json')
    response.end(strProduct) */
    response.send(products)
})
//create
router.post('/product',(request,response)=>{
    const body = request.body
    /* console.log(body) */
    const id = body.id
    const title = body.title
    const description = body.description
    const price = body.price

    //Inserting object into products array
    products.push({
        id:id,
        title:title,
        description:description,
        price:price
    })
    response.send('Product Added')
})
//update
router.put('/product/:id',(request,response)=>{
    const body = request.body
    /* console.log(body) */
    const id = request.params.id
    const price = body.price

    for (let index = 0; index < products.length; index++) {
        const product = products[index];
        if (product.id == id) {
            product.price = price
        }
    }

    response.send('Product price Updated')
})

//delete
router.delete('/product/:id',(request,response)=>{
    const id = request.params.id
    for (let index = 0; index < products.length; index++) {
        const product = products[index];
        if (product.id == id) {
            console.log(product)
            products.splice(index,1)
            break
        }
    }
    response.send('product is deleted')
})
module.exports = router