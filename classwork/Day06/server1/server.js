const express = require('express')
const app = express()
const routerProduct = require('./routes/product')
const bodyParser = require('body-parser')

app.use(bodyParser.json())
app.use(routerProduct)
app.listen(4433,'localhost',()=>{
    console.log('server stated on port 4433')
})