let num = 100 //we can update value
console.log(`num = ${num}`)
num = 200
console.log(`num = ${num}`)

const pi = 3.14 //cannot update value of pi
console.log(`pi = ${pi}`)
