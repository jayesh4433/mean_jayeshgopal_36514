function display() {
   const numbers = [10, 20, 30, 40, 50] //array of numbers
   console.log(numbers) 
   console.log(`typeof(numbers) =   ${typeof(numbers)}`)

/* //    iterate over the array  - traditional method
   for (let index = 0; index < numbers.length; index++) {
       console.log(`numbers[${index}]   =   ${numbers[index]}`)       
   } */

//    iterate over the array  - foreach loop
   for (let number of numbers) {
    console.log(number)
   }


}
/* display() */

function display2() {
    const students = ["Jayesh",'Prabodh',`Akash`]
    console.log(students) 
    console.log(`typeof(students) =   ${typeof(students)}`)

    for (const student of students) {
        console.log(student)
        
    }
}
/* display2() */


function display3() {
    const numbers = [10, 20, 30, 40, 50] //array of numbers
    console.log(numbers) 
    //console.log(`typeof(numbers) =   ${typeof(numbers)}`)
    numbers.push(60)
    numbers.push(70)
    numbers.push(80)
    console.log(numbers)
    const lastValue = numbers.pop()
    console.log(`last value = ${lastValue}`)
    console.log(numbers)

    //delete value by using index position
    numbers.splice(3,2)
    console.log(numbers)
}

display3();

function display4() {
    const students = ["Jayesh",'Prabodh',`Akash`,"Ajinkya"]
    console.log(students) 
    console.log(`typeof(students) =   ${typeof(students)}`)
    students.push("tejas")
    students.push("vinay")
    students.push("chinmay")
    students.push("abcd")
    console.log(students)
    const lastStudent = students.pop()
    console.log(`last student = ${lastStudent}`)

}
display4();