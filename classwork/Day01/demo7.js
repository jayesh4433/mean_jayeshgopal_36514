function f1() {
    const numbers = [10,20,30,40,50,60,70,80]

    /* //traditional for loop
    for (let index = 0; index < numbers.length; index++){
        console.log(`numbers[${index}]   =   ${numbers[index]}`)
    }

    console.log() */

   /*  //for...of loop
    for (const number of numbers) {
        console.log(`number =   ${number}`)
        
    }
    console.log() */

   //foreach loop
   numbers.forEach((number) => {
    console.log(`number =   ${number}`)
   });
}

/* f1() */


function f2(p1) {
    console.log("Inside f2()")
    console.log(`p1 = ${p1}  typeof(p1) = ${typeof(p1)}`)
    p1()
  }

  /* f2(10) */
  /* f2("Hello") */
  /* f2(false) */
  /* f2() */
  /* f2(f1) */
  /* f2(()=>{
      console.log("Inside Arrow Function")
  }) */

  function f3(){
      const persons = [
          {name:"Jayesh", address:"Pune", age:23},
          {name:"Prabodh", address:"Chiplun", age:23},
          {name:"Akash", address:"Indoli", age:23}
      ]
      persons.forEach(person => {
          console.log(`name:${person['name']}, address:${person['address']}, age:${person['age']}`)
      });
  }
  /* f3() */

  function f4() {
      const numbers = [1,2,3,4,5,6,7,8,9]
      const squares = []

      numbers.forEach(number => {
        console.log(`${number} = ${number*number}`)
    });

      numbers.forEach(number => {
          squares.push(number*number)
      });

      console.log(numbers)
      console.log(squares)
  }
  f4()