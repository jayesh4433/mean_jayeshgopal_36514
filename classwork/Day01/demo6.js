let num = 100
/* console.log(`num = ${num} type = ${typeof(num)}`) */

let num2 = num
/* console.log(`num2 = ${num2} type = ${typeof(num2)}`) */

num = 500
/* console.log(`num = ${num} type = ${typeof(num)}`)
console.log(`num2 = ${num2} type = ${typeof(num2)}`) */

function f1() {
    console.log("Inside f1()")
}

/* console.log(`f1 = ${f1}, type = ${typeof(f1)}`) */

//Function alias
const myfun = f1
/* console.log(`myfun = ${myfun}, type = ${typeof(myfun)}`) */

//anonymous function
const f2 = function () {
    console.log("Inside function f2()")
    
}
f2()

//anonymous function
//big fat arrow / arrow function
const f3 = () => {
    console.log("Inside function f3()")
}
f3()

const square = (p) => { 
    console.log(`square of ${p} = ${p*p}`)
}
square(11)

const add = (p1,p2) => {
    console.log(`Addition = ${p1 + p2}`)
}
add(44,33)