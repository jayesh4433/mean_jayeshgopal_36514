function display() {
  console.log("This is parameter less Function")

}
// display();

 function myDisplay(p1) {
   console.log('This is parameterized function')
   console.log(`p1 = ${p1}  typeof(p1) = ${typeof(p1)}`)
   
 }
/*  myDisplay(20)
 myDisplay("Hello World")
 myDisplay(44.33) */

 function add(p1,p2) {
   let addition = p1 + p2 
   console.log(`addition = ${addition}`)
   
 }

 /* add(44,33)
 add(44,'33')
 add(44,`33`) */

 function myDisplay2(p1) {
  console.log("Inside myDisplay2 Function")
  console.log(`p1 = ${p1}  typeof(p1) = ${typeof(p1)}`)
}

/* myDisplay2()
myDisplay2(44)
myDisplay2(44,33) //33 will discarded */

function add2() {

  console.log(arguments)
  let sum = 0
  for (const number of arguments) {
    sum += number
  }
  console.log(`Sum = ${sum}`)
}

add2()
add2(44)
add2(44,33)
add2(44,33,32,55)

