const num = 100 //type = number
console.log(`num = ${num}, type = ${typeof(num)}`)

const salary = 50.50 //type = number
console.log(`salary = ${salary}, type = ${typeof(salary)}`)

const firstName = 'Jayesh' //type = string
console.log(`firstName = ${firstName}, type = ${typeof(firstName)}`)

const lastName = "Gopal" //type = string
console.log(`lastName = ${lastName}, type = ${typeof(lastName)}`)

//type = string
const address = ` 
building name,
city,
state,
country,
zipCode`
console.log(`address = ${address}, type = ${typeof(address)}`)

const canVote = true  //type = Boolean
console.log(`canVote = ${canVote}, type = ${typeof(canVote)}`)

let myvar //type = undefined
console.log(`myvar = ${myvar}, type = ${typeof(myvar)}`)

const person = { name: "Jayesh", address: "pune", phone: "+914433" } //type = object
console.log(`person = ${person}, type = ${typeof(person)}`)
console.log(person)
console.log(person['name']+"    "+person['address']+"   "+person['phone'])
