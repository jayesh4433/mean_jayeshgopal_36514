"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.pi = exports.Maths = void 0;
var Maths = /** @class */ (function () {
    //ctor
    function Maths(num1, num2) {
        if (num1 === void 0) { num1 = 0; }
        if (num2 === void 0) { num2 = 0; }
        this._num1 = num1;
        this._num2 = num2;
    }
    Object.defineProperty(Maths.prototype, "num1", {
        //getter
        get: function () { return this._num1; },
        //setter
        set: function (num) { this._num1 = num; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Maths.prototype, "num2", {
        get: function () { return this._num2; },
        set: function (num) { this._num2 = num; },
        enumerable: false,
        configurable: true
    });
    //methods
    Maths.prototype.add = function () {
        return this._num1 + this._num2;
    };
    Maths.prototype.sub = function () {
        return this._num1 - this._num2;
    };
    Maths.prototype.div = function () {
        return this._num1 / this._num2;
    };
    Maths.prototype.mul = function () {
        return this._num1 * this._num2;
    };
    return Maths;
}());
exports.Maths = Maths;
exports.pi = 3.14;
