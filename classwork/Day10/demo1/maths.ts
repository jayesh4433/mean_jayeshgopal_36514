export class Maths{
   private _num1 : number
   private _num2 : number

   //ctor
   constructor(num1:number=0,num2:number=0)
   {
       this._num1 = num1
       this._num2 = num2
   }

   //setter
   public set num1(num: number){this._num1 = num}
   public set num2(num: number){this._num2 = num}

   //getter
   public get num1(){return this._num1}
   public get num2(){return this._num2}


   //methods
   public add():number{
       return this._num1+this._num2
   }
   public sub():number{
        return this._num1-this._num2
    }
    public div():number{
        return this._num1/this._num2
    }
    public mul():number{
        return this._num1*this._num2
    }
}

export const pi = 3.14