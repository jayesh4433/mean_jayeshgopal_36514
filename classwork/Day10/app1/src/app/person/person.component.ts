import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-person',
  templateUrl: './person.component.html',
  styleUrls: ['./person.component.css']
})
export class PersonComponent implements OnInit {
  personName = 'Jayesh Gopal'
  personAddress = 'Pune'
  personAge = 23
  personPhone = '+914242'
  personEmail = 'jayesh@gmail.com'
  constructor() { }

  ngOnInit(): void {
  }

}
