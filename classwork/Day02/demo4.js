const person1 = {
    /* properties */
    name: 'Jayesh', 
    address: 'Pune', 
    age: '23', 

    /* method */
    canVote : function () {
        if (this['age'] >= 18) {
          console.log('person is eligible for voting')
        } else {
          console.log('person is not eligible for voting')
        }
    },
    printInfo : function (){
        console.log(`Name   :   ${this.name}`)
        console.log(`Address:   ${this.address}`)
        console.log(`Age    :   ${this.age}`)
    }
}
person1.canVote()
person1.printInfo()



const person2 = {
    /* properties */
    name: 'Prabodh', 
    address: 'Chiplun', 
    age: '17', 

    /* method */
    canVote : function () {
        if (this['age'] >= 18) {
          console.log('person is eligible for voting')
        } else {
          console.log('person is not eligible for voting')
        }
    },
    printInfo : function (){
        console.log(`Name   :   ${this.name}`)
        console.log(`Address:   ${this.address}`)
        console.log(`Age    :   ${this.age}`)
    }
}
person2.canVote()
person2.printInfo()