function person(name,address,age) {
    this.name = name
    this.address = address
    this.age = age
}

const person1 = new person('Jayesh','Pune',23)
console.log(person1)

function Mobile(model,company,price) {
    this.model = model
    this.company = company
    this.price = price
}

const mobile1 = new Mobile('iPhone XS Max', 'Apple', 144000)
console.log(mobile1)