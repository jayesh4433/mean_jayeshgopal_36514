function f1()
{
    const numbers = [1,2,3,4,5,6,7,8,9,10,11]
    const evenNumbers = []

    for (let index = 0; index < numbers.length; index++) {
        const number = numbers[index];
        if(number%2 == 0)
        {
            evenNumbers.push(number)
        }
    }
    console.log("Inside f1()")
    console.log(numbers)
    console.log(evenNumbers)
}
/* f1() */



function f2()
{
    const numbers = [1,2,3,4,5,6,7,8,9,10,11]
    const evenNumbers = []

    numbers.forEach(number=>{
        if(number%2 == 0)
        {
            evenNumbers.push(number)
        }
    })

    console.log("Inside f2()")
    console.log(numbers)
    console.log(evenNumbers)
}
/* f2() */



function f3()
{
    const numbers = [1,2,3,4,5,6,7,8,9,10,11]


    const evenNumbers = numbers.filter(number=>{
        return number%2 == 0
    })

    const oddNumbers = numbers.filter(number=>{
        return number%2 != 0
    })

    console.log("Inside f3()")
    console.log(numbers)
    console.log(evenNumbers)
    console.log(oddNumbers)
}
/* f3() */



function f4()
{
    const cars = [
        { id: 1, model: 'i20', company: 'hyundai', price: 7.5 },
        { id: 2, model: 'i10', company: 'hyundai', price: 5.5 },
        { id: 3, model: 'fabia', company: 'skoda', price: 6.5 },
        { id: 4, model: 'nano', company: 'tata', price: 2.5 },
        { id: 5, model: 'X5', company: 'BMW', price: 40.0 },
        { id: 6, model: 'Autobiography', company: 'Ranag Rover', price: 95.5 }
    ]
    
    const affordableCars = []

    cars.forEach(car => {
       if(car['price'] < 10){affordableCars.push(car)}
    })
    
    console.log("Inside f4()")
    console.log(cars)
    console.log(affordableCars)
}
/* f4() */


function f5()
{
    const cars = [
        { id: 1, model: 'i20', company: 'hyundai', price: 7.5 },
        { id: 2, model: 'i10', company: 'hyundai', price: 5.5 },
        { id: 3, model: 'fabia', company: 'skoda', price: 6.5 },
        { id: 4, model: 'nano', company: 'tata', price: 2.5 },
        { id: 5, model: 'X5', company: 'BMW', price: 40.0 },
        { id: 6, model: 'Autobiography', company: 'Ranag Rover', price: 95.5 }
    ]
    
    const affordableCars = cars.filter(car => {
        return car['price'] < 10
    })
    
    console.log("Inside f5()")
    console.log(cars)
    console.log(affordableCars)
}
/* f5() */



function f6()
{
    const cars = [
        { id: 1, model: 'i20', company: 'hyundai', price: 7.5 },
        { id: 2, model: 'i10', company: 'hyundai', price: 5.5 },
        { id: 3, model: 'fabia', company: 'skoda', price: 6.5 },
        { id: 4, model: 'nano', company: 'tata', price: 2.5 },
        { id: 5, model: 'X5', company: 'BMW', price: 40.0 },
        { id: 6, model: 'Autobiography', company: 'Ranag Rover', price: 95.5 }
    ]
    
    const affordableCars = cars.filter(car => {
        return car['price'] < 10
    }).map(car => { return {model: car['model'],company: car['company']} })
    
    console.log("Inside f6()")
    console.log(cars)
    console.log(affordableCars)
}
f6()