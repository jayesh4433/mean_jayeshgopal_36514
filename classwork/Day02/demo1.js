function f1()
{
    const numbers = [1,2,3,4,5,6,7,8,9,10,11]
    const square = []

    for (let index = 0; index < numbers.length; index++) {
        square.push(numbers[index]*numbers[index])
    }
    console.log("Inside f1()")
    console.log(numbers)
    console.log(square)
}
/* f1() */


function f2()
{
    const numbers = [1,2,3,4,5,6,7,8,9,10,11]
    const square = []

    numbers.forEach(number =>{
        square.push(number*number)
    })
    console.log("Inside f2()")
    console.log(numbers)
    console.log(square)
}
/* f2() */



function f3()
{
    const numbers = [1,2,3,4,5,6,7,8,9,10,11]

    const square = numbers.map(number => number*number)
    
    console.log("Inside f3()")
    console.log(numbers)
    console.log(square)
}
/* f3() */



function f4()
{
    const numbers = [1,2,3,4,5,6,7,8,9,10,11]

    const cubes = numbers.map(number => number*number*number)
    
    console.log("Inside f4()")
    console.log(numbers)
    console.log(cubes)
}
/* f4() */



function f5()
{
    const temperaturesC = [30, 31, 34, 35, 37, 38, 25]

    const temperaturesF = temperaturesC.map(tempc => (tempc * (9/5)) + 32)
    
    console.log("Inside f5()")
    console.log(temperaturesC)
    console.log(temperaturesF)
}
/* f5() */



function f6()
{
    const cars = [
        { id: 1, model: 'i20', company: 'hyundai', price: 7.5 },
        { id: 2, model: 'i10', company: 'hyundai', price: 5.5 },
        { id: 3, model: 'fabia', company: 'skoda', price: 6.5 },
        { id: 4, model: 'nano', company: 'tata', price: 2.5 },
        { id: 5, model: 'X5', company: 'BMW', price: 40.0 },
        { id: 6, model: 'Autobiography', company: 'Ranag Rover', price: 95.5 }
    ]
    
    const newCars = []

    for (let index = 0; index < cars.length; index++) {
        const car = cars[index];

        newCars.push({
            model: car['model'],company: car['company']
        })
    }
    
    console.log("Inside f6()")
    console.log(cars)
    console.log(newCars)
}
/* f6() */



function f7()
{
    const cars = [
        { id: 1, model: 'i20', company: 'hyundai', price: 7.5 },
        { id: 2, model: 'i10', company: 'hyundai', price: 5.5 },
        { id: 3, model: 'fabia', company: 'skoda', price: 6.5 },
        { id: 4, model: 'nano', company: 'tata', price: 2.5 },
        { id: 5, model: 'X5', company: 'BMW', price: 40.0 },
        { id: 6, model: 'Autobiography', company: 'Ranag Rover', price: 95.5 }
    ]
    
    const newCars = []

    cars.forEach(car => {
        newCars.push({
            model: car['model'],company: car['company']
        })
    })

    console.log("Inside f7()")
    console.log(cars)
    console.log(newCars)
}
/* f7() */



function f8()
{
    const cars = [
        { id: 1, model: 'i20', company: 'hyundai', price: 7.5 },
        { id: 2, model: 'i10', company: 'hyundai', price: 5.5 },
        { id: 3, model: 'fabia', company: 'skoda', price: 6.5 },
        { id: 4, model: 'nano', company: 'tata', price: 2.5 },
        { id: 5, model: 'X5', company: 'BMW', price: 40.0 },
        { id: 6, model: 'Autobiography', company: 'Ranag Rover', price: 95.5 }
    ]
    
    const newCars = cars.map(car => { return {model: car['model'],company: car['company']} })

    console.log("Inside f8()")
    console.log(cars)
    console.log(newCars)
}
f8()