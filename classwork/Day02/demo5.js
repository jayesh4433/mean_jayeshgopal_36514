function canVote () {
    if (this['age'] >= 18) {
      console.log('person is eligible for voting')
    } else {
      console.log('person is not eligible for voting')
    }
}
 function printInfo (){
    console.log(`Name   :   ${this.name}`)
    console.log(`Address:   ${this.address}`)
    console.log(`Age    :   ${this.age}`)
}

const person1 = {
    /* properties */
    name: 'Jayesh', 
    address: 'Pune', 
    age: '23', 

    /* method */
    canVote : canVote,
    printInfo : printInfo
}
person1.canVote()
person1.printInfo()



const person2 = {
    /* properties */
    name: 'Prabodh', 
    address: 'Chiplun', 
    age: '17', 

    /* method */
    canVote : canVote,
    printInfo : printInfo
}
person2.canVote()
person2.printInfo()