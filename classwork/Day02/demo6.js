const person1 = new Object()

person1.name = "Jayesh"
person1.address = "Pune"
person1.age = 23

console.log(`Name   :   ${person1.name}`)
console.log(`Address:   ${person1.address}`)
console.log(`Age    :   ${person1.age}`)
console.log('-------------------------------------')

const person2 = new Object()

person2['name'] = "Jayesh"
person2['address'] = "Pune"
person2['age'] = 23

console.log(`Name   :   ${person2.name}`)
console.log(`Address:   ${person2.address}`)
console.log(`Age    :   ${person2.age}`)
console.log('-------------------------------------')

console.log(person1)
console.log(person2)

const mobile1 = new Object()

mobile1.model = 'iPhone XS Max'
mobile1.company = 'Apple'
mobile1.price = 144000

console.log(mobile1)