function canVote(person) {
    if (person['age'] >= 18) {
      console.log('person is eligible for voting')
    } else {
      console.log('person is not eligible for voting')
    }
  }  

function f1() {
    const person1 = {name: 'Jayesh', address: 'Pune', age: '23'}
    console.log(person1)
    console.log(`typeof person = ${typeof(person1)}`)
    
    canVote(person1)
    //person1.canVote()

    const person2 = {name: 'Prabodh', address: 'Chiplun', age: '17'}
    console.log(person2)
    console.log(`typeof person = ${typeof(person2)}`)
    
    canVote(person1)
    //person2.canVote()
}
f1()