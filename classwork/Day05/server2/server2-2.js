const express = require('express')
const { request, response } = require('express')

const app = express()

function log1(request,response,next) {
    console.log('Inside Log1 Function')
    next()
}

const log2 = function(request,response,next) {
    console.log('Inside Log2 Function')
    next()
}

const log3 = (request,response,next) => {
    console.log('Inside arrow Log3 Function')
    next()
}

app.use(log3)

app.use(log1)
app.use((request,response,next) => {
    console.log('Inside arrow log Function')
    next()
})

app.get('/',(request,response)=>{
    console.log('Inside GET')
    const products = [
        { id: 1, title: 'product 1', price: 100 },
        { id: 2, title: 'product 2', price: 200 },
        { id: 3, title: 'product 3', price: 300 }
    ]
    const strProducts = JSON.stringify(products)

    response.setHeader('Content-type', 'application/json')
    response.end(strProducts)
    
    /* response.end('This is GET /') */
})
app.post('/',(request,response)=>{
    console.log('Inside POST')
    response.end('This is POST /')
})
app.put('/',(request,response)=>{
    console.log('Inside PUT')
    response.end('This is PUT /')
})
app.delete('/',(request,response)=>{
    console.log('Inside DELETE')
    response.end('This is DELETE /')
})

/* app.use(log2)//This will not get call */

app.listen('4433','0.0.0.0',()=>{
    console.log('Server started on port no 4433')
})

/* app.use(log2)//This will not get call */