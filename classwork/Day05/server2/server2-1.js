const express = require('express')
const { request, response } = require('express')

const app = express()

function log(request,response,next) {
    console.log('Inside Log Function')
    console.log(`method = ${request.method}`)
    console.log(`url = ${request.url}`)
    next()
}

app.use(log)

app.get('/',(request,response)=>{
    response.end('This is GET /')
})
app.post('/',(request,response)=>{
    response.end('This is POST /')
})
app.put('/',(request,response)=>{
    response.end('This is PUT /')
})
app.delete('/',(request,response)=>{
    response.end('This is DELETE /')
})
app.listen('4433','0.0.0.0',()=>{
    console.log('Server started on port no 4433')
})