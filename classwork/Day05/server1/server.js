const express = require('express')

const app = express()

const userRouter = require('./routes/user')
const orderRouter = require('./routes/order')
const productRouter = require('./routes/product')


app.get('/',(request,response)=>{
    response.end('This is GET /')
})

app.use(userRouter)
app.use(orderRouter)
app.use(productRouter)

app.listen('4433','0.0.0.0',()=>{
    console.log('Server started on port no 4433')
})

