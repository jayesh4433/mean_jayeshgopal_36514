const express = require('express')
const { response, request } = require('express')

const app = express()

// Routes
app.get('/',(request,response)=>{
    response.end('This is GET /')
})
app.post('/',(request,response)=>{
    response.end('This is POST /')
})
app.put('/',(request,response)=>{
    response.end('This is PUT /')
})
app.delete('/',(request,response)=>{
    response.end('This is DELETE /')
})

//Routes For Product
app.get('/product',(request,response)=>{
    console.log('select * from product')
    response.end('This is GET /product')
})
app.post('/',(request,response)=>{
    console.log('insert into product (...)')
    response.end('This is POST /product')
})
app.put('/',(request,response)=>{
    console.log('update product...')
    response.end('This is PUT /product')
})
app.delete('/',(request,response)=>{
    console.log('delete from product')
    response.end('This is DELETE /product')
})

//Routes For Order
app.get('/order',(request,response)=>{
    console.log('select * from order')
    response.end('This is GET /order')
})
app.post('/',(request,response)=>{
    console.log('insert into order (...)')
    response.end('This is POST /order')
})
app.put('/',(request,response)=>{
    console.log('update order...')
    response.end('This is PUT /order')
})
app.delete('/',(request,response)=>{
    console.log('delete from order')
    response.end('This is DELETE /order')
})

//Routes For Category
app.get('/category',(request,response)=>{
    console.log('select * from category')
    response.end('This is GET /category')
})
app.post('/',(request,response)=>{
    console.log('insert into category (...)')
    response.end('This is POST /category')
})
app.put('/',(request,response)=>{
    console.log('update category...')
    response.end('This is PUT /category')
})
app.delete('/',(request,response)=>{
    console.log('delete from category')
    response.end('This is DELETE /category')
})

app.listen('4433','0.0.0.0',()=>{
    console.log('Server started on port no 4433')
})