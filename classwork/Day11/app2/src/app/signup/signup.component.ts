import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  firstName = ''
  lastName = ''
  address = ''
  phone = ''
  email = ''
  password = ''

  constructor() { }

  ngOnInit(): void {
  }
  onSignUp(){
    console.log(`first name : ${this.firstName}`)
    console.log(`last name : ${this.lastName}`)
    console.log(`address : ${this.address}`)
    console.log(`phone : ${this.phone}`)
    console.log(`email : ${this.email}`)
    console.log(`password : ${this.password}`)
  }
  onCancel(){
    this.firstName = ''
    this.lastName = ''
    this.address = ''
    this.phone = ''
    this.email = ''
    this.password = ''
  }

}
