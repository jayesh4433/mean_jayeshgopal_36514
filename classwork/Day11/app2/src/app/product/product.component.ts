import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {
  
  title = ''
  price = 0
  description = ''
  company = 'Apple'

  products = [
    
  ]
  constructor() { }

  ngOnInit(): void {
  }

  onAdd(){

    this.products.push({
      title: this.title,
      description: this.description,
      price: this.price,
      company: this.company
    })

    //clear fields after adding product
    this.onCancel()
  }
  onCancel(){
    this.title = ''
    this.price = 0
    this.description = ''
    this.company = 'Apple'
  }

  onDelete(index){
    this.products.splice(index,1)
  }

}
