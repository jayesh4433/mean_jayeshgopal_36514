import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-first',
  templateUrl: './first.component.html',
  styleUrls: ['./first.component.css']
})
export class FirstComponent implements OnInit {

  personName = 'Jayesh Gopal'
  personAge = 23
  personAddress = 'Pune'
  personPhone = '+914242'

  person = {
    name: 'Prabodh Kherade',
    age: 23,
    address: 'Chiplun',
    phone: '+914242',
    email:'prabodh@gmail.com'
  }
  constructor() { }

  ngOnInit(): void {
  }

}
