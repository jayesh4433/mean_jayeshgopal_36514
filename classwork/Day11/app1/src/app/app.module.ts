import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { FirstComponent } from './A_first/first.component';
import { SecondComponent } from './B_second/second.component';
import { ThirdComponent } from './C_third/third.component';
import { FourthComponent } from './D_fourth/fourth.component';
import { FifthComponent } from './E_fifth/fifth.component';
import { SixthComponent } from './F_sixth/sixth.component';
import { SeventhComponent } from './G_seventh/seventh.component';
import { EighthComponent } from './H_eighth/eighth.component';

@NgModule({
  declarations: [
    AppComponent,
    FirstComponent,
    SecondComponent,
    ThirdComponent,
    FourthComponent,
    FifthComponent,
    SixthComponent,
    SeventhComponent,
    EighthComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
