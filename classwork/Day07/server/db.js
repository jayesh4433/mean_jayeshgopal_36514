const mysql = require('mysql2')
//create mysql pool
const pool = mysql.createPool({
    host: 'localhost',
    user: 'mean',
    password: 'mean',
    database: 'mystore',
    port: 3306,
    waitForConnections: true,
    connectionLimit: 10,
    queueLimit: 0
  })
  
  module.exports = {
    connnection: pool
  }