const express = require('express')
const db = require('../db')
const crypto = require('crypto-js')
const utils = require('../utils')
const router = express.Router()

router.post('/signup',(request,response)=>{
    const {firstName,lastName,email,password,address,phone} = request.body
    const encryptedPass = crypto.SHA256(password)
    const statement = 'insert into user(firstName,lastName,email,password,address,phone)'+ 
    ` values ( '${firstName}','${lastName}','${email}','${encryptedPass}','${address}','${phone}')`

    db.connnection.query(statement,(error,data)=>{
        response.send(utils.createResult(error,data))
    })
})
router.post('/login',(request,response)=>{
    const{email,password} = request.body
    const encryptedPass = crypto.SHA256(password)
    const statement = `select firstName,lastName,address,phone from user where email='${email}' and password='${encryptedPass}'`

    db.connnection.query(statement,(error,users)=>{
        const result = {}
        if(error)
        {
            result['status'] = 'error'
            result['error'] = error
        }
        else
        {
            if(users.length == 0)
            {
                result['status'] = 'error'
                result['error'] = 'Invalid credentials'
            }
            else
            {
                result['status'] = 'success'
                result['data'] = users[0]
            }
        }
        response.send(result)
    })
})

module.exports = router