const express = require('express')
const db = require('../db')
const utils = require('../utils')
const router = express()

router.get('/',(request,response)=>{
    const statement = 'select * from category'
    
    db.connnection.query(statement,(error,categories)=>{
        response.send(utils.createResult(error,categories))
    })
})
router.post('/',(request,response)=>{
    const {title,description} = request.body
    const statement = 'insert into category (title,description) '+
    `values ('${title}','${description}')`
    db.connnection.query(statement,(error,categories)=>{
        response.send(utils.createResult(error,categories))
    })
})

module.exports = router