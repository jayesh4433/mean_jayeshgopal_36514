const express = require('express')
const app = express()
const routerUser = require('./routes/user')
const routerCategory = require('./routes/category')
const bodyParser = require('body-parser')


app.get('/',(request,response)=>{
    response.send('<h1>Welcome</h1>')
})

app.use(bodyParser.json())
app.use('/user',routerUser)
app.use('/category',routerCategory)

app.listen(4433,'localhost',()=>{
    console.log('Server started on port 4433')
})
