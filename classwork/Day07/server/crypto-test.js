const crypto = require('crypto-js')

const password = 'jayesh1234'

console.log(`plain password     = ${password}`)
console.log(`SHA1 password      = ${crypto.SHA1(password)}, length = ${crypto.SHA1(password).toString().length}`)
console.log(`SHA3 password      = ${crypto.SHA3(password)}, length = ${crypto.SHA3(password).toString().length}`)
console.log(`SHA256 password    = ${crypto.SHA256(password)}, length = ${crypto.SHA256(password).toString().length}`)
console.log(`SHA512 password    = ${crypto.SHA512(password)}, length = ${crypto.SHA512(password).toString().length}`)
console.log(`MD5 password       = ${crypto.MD5(password)}, length = ${crypto.MD5(password).toString().length}`)