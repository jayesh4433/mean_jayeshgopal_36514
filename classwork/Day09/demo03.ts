//Explicit variable declaration [with data type]
const num: number = 10
console.log(`num = ${num}, type = ${typeof(num)}`)

const salary:number = 15.20
console.log(`salary = ${salary}, type = ${typeof(salary)}`)

const str:string = "Hello"
console.log(`str = ${str}, type = ${typeof(str)}`)

const canVote:boolean = true
console.log(`canVote = ${canVote}, type = ${typeof(canVote)}`)

let myvar:undefined = undefined
console.log(`myvar = ${myvar}, type = ${typeof(myvar)}`)

//Generic Object
const person:object = {name:'Jayesh Gopal', age:23}
console.log(`person = ${person}, type = ${typeof(person)}`)

//Explicit Object
const mobile:{model:string, price:number} = {model:"One Plus 7",price:33000}
console.log(`mobile = ${mobile}, type = ${typeof(mobile)}`)

//Union of string and number
let myvar2:string|number = 'test'
myvar2 = 500
//myvar2 = false //Not Ok

//Union of string & number  & boolean & object & undefined
let myvar3:string|number|boolean|object|undefined = "Hello World"
myvar3 = 6450
myvar3 = false
myvar3 = {name:"Jayesh",age:23}
myvar3 = undefined


//store any value
let myvar4:any = "Hello World"
myvar4 = 6450
myvar4 = false
myvar4 = {name:"Jayesh",age:23}
myvar4 = undefined