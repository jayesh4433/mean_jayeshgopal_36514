//Implicit declaration
const num = 10
console.log(`num = ${num}, type = ${typeof(num)}`)

const salary = 15.20
console.log(`salary = ${salary}, type = ${typeof(salary)}`)

const str = "Hello"
console.log(`str = ${str}, type = ${typeof(str)}`)

const canVote = true
console.log(`canVote = ${canVote}, type = ${typeof(canVote)}`)

const person = {name:'Jayesh Gopal', age:23}
console.log(`person = ${person}, type = ${typeof(person)}`)

let myvar
console.log(`myvar = ${myvar}, type = ${typeof(myvar)}`)
