//Super class
class Person{
    protected _name : string
    protected _address :string
    protected _age : number

    //constructor
    public constructor(name:string = '',address:string = '',age:number = 0){
        console.log('Inside Person class ctor')
        this._name = name
        this._address = address
        this._age = age
    }

    //facilitator
    public printInfo(){
        console.log(`name : ${this._name}`)
        console.log(`address : ${this._address}`)
        console.log(`age : ${this._age}`)
    }
}

//Sub class
class Employee extends Person{
    //property
    protected _id : number
    protected _company : string

    //constructor
    public constructor(id:number,name:string,address:string,age:number,company:string){
        super(name,address,age)
        this._id = id
        this._company = company
    }

    //facilitator
    public printInfo(){
        console.log(`id : ${this._id}`)
        console.log(`company : ${this._company}`)
        // console.log(`name : ${this._name}`)
        // console.log(`address : ${this._address}`)
        // console.log(`age : ${this._age}`)
        super.printInfo()//call base class printInfo method
    }
}

const p1 = new Person('Jayesh Gopal','Pune',23)
p1.printInfo()
//console.log(p1)
console.log('---------------------------------------')
const e1 = new Employee(1,'Employee1','Pune',25,'company1')
//console.log(e1)
e1.printInfo()