function addv1(p1, p2) {
    console.log("additionV1 = " + (p1 + p2));
}
// addv1(10,20)
// addv1(10,'20')
function addv2(p1, p2) {
    console.log("additionV2 = " + (p1 + p2));
}
// addv2(10,20)
// addv2(10,'20')//Not Allowed
function addv3(p1, p2) {
    return '' + p1 + p2;
}
// const result = addv3(10,20)
// console.log(`add : ${result}, square : ${result * result}`)
function addv4(p1, p2) {
    //return '' + p1 + p2 not allowed
    return p1 + p2;
}
// const result = addv4(10,20)
// console.log(`add : ${result}, square : ${result * result}`)
function sub(p1, p2) {
    return p1 - p2;
}
var result = sub(20, 10);
console.log("Sub Result = " + result);
function concatination(str1, str2) {
    return str1 + str2;
}
var concat = concatination("Hello", "Hell.....");
console.log("concat string = " + concat);
function cube(p1) {
    return p1 * p1 * p1;
}
console.log("cube = " + cube(10));
