var Person = /** @class */ (function () {
    //Constructor
    function Person() {
        console.log('Inside Person Constructor');
        this.name = '';
        this.address = '';
        this.age = 0;
    }
    //method
    Person.prototype.printInfo = function () {
        console.log("Name : " + this.name);
        console.log("Age : " + this.age);
        console.log("Address : " + this.address);
    };
    return Person;
}());
//Instantiation of Person class
var p1 = new Person();
//Initializing the object
p1.name = 'Jayesh Gopal';
p1.age = 23;
p1.address = 'Pune';
//calling methods
p1.printInfo();
console.log(p1);
