interface Shape{
    //list of abstract method
    erase()
    draw()
}

class Rectangle implements Shape{
    erase() { console.log('rectangle is getting erased') }
    draw() { console.log('rectangle is getting draw') }
}

class Circle implements Shape{
    erase() { console.log('circle is getting erased') }
    draw() { console.log('circle is getting draw') }
}

class Square implements Shape{
    erase() { console.log('Square is getting erased') }
    draw() { console.log('Square is getting draw') }
}

function performOperation(shape: Shape)
{
    shape.draw()
    shape.erase()
}

const r1 = new Rectangle()
performOperation(r1)
const c1 = new Circle()
performOperation(c1)
const s1 = new Square()
performOperation(s1)