class Person {
//property
    name:string
    age:number
    address:string

//Constructor
constructor(){
    console.log('Inside Person Constructor')
    this.name = ''
    this.address = ''
    this.age = 0
}

//method
    printInfo(){
        console.log(`Name : ${this.name}`)
        console.log(`Age : ${this.age}`)
        console.log(`Address : ${this.address}`)
    }
}

//Instantiation of Person class
const p1 = new Person()

//Initializing the object
p1.name = 'Jayesh Gopal'
p1.age = 23
p1.address = 'Pune'

//calling methods
p1.printInfo()
console.log(p1)