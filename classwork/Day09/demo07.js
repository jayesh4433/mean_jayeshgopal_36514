var Person = /** @class */ (function () {
    //Constructor
    //can not overload ctor / method
    // constructor(){
    //     console.log('Inside default Constructor')
    //     this.name = ''
    //     this.address = ''
    //     this.age = 0
    // }
    // /* constructor(name:string,address:string,age:number){
    //     console.log('Inside Parameterize Constructor')
    //     this.name = name
    //     this.address = address
    //     this.age = age
    // } */
    function Person(name, address, age) {
        if (name === void 0) { name = ''; }
        if (address === void 0) { address = ''; }
        if (age === void 0) { age = 0; }
        console.log('Inside Constructor');
        this.name = name;
        this.address = address;
        this.age = age;
    }
    //method
    Person.prototype.printInfo = function () {
        console.log("Name : " + this.name);
        console.log("Age : " + this.age);
        console.log("Address : " + this.address);
    };
    return Person;
}());
//Instantiation and Initializing object of Person class
var p1 = new Person('Jayesh Gopal', 'Pune', 23);
p1.printInfo();
var p2 = new Person();
p2.printInfo();
