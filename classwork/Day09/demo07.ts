class Person {
//property
    name:string
    age:number
    address:string

//Constructor
//can not overload ctor / method
// constructor(){
//     console.log('Inside default Constructor')
//     this.name = ''
//     this.address = ''
//     this.age = 0
// }


// /* constructor(name:string,address:string,age:number){
//     console.log('Inside Parameterize Constructor')
//     this.name = name
//     this.address = address
//     this.age = age
// } */

constructor(name:string = '',address:string = '',age:number = 0){
    console.log('Inside Constructor')
    this.name = name
    this.address = address
    this.age = age
}

//facilitator
    printInfo(){
        console.log(`Name : ${this.name}`)
        console.log(`Age : ${this.age}`)
        console.log(`Address : ${this.address}`)
    }
}

//Instantiation and Initializing object of Person class
const p1 = new Person('Jayesh Gopal','Pune',23)
p1.printInfo()

const p2 = new Person()
p2.printInfo()