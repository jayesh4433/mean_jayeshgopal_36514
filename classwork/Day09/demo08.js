var Person = /** @class */ (function () {
    //Constructor
    function Person(name, address, age) {
        if (name === void 0) { name = ''; }
        if (address === void 0) { address = ''; }
        if (age === void 0) { age = 0; }
        console.log('Inside Constructor');
        this.name = name;
        this.address = address;
        this.age = age;
    }
    //getter
    Person.prototype.getName = function () { return this.name; };
    Person.prototype.getAddress = function () { return this.address; };
    Person.prototype.getAge = function () { return this.age; };
    //setter
    Person.prototype.setName = function (name) { this.name = name; };
    Person.prototype.setAddress = function (address) { this.address = address; };
    Person.prototype.setAge = function (age) {
        if ((0 < age) && (age < 100)) {
            this.age = age;
        }
        else {
            console.log('Age is not valid');
        }
    };
    //facilitator
    Person.prototype.printInfo = function () {
        console.log("Name : " + this.name);
        console.log("Age : " + this.age);
        console.log("Address : " + this.address);
    };
    return Person;
}());
var p1 = new Person('Jayesh Gopal', 'Pune', 23);
p1.printInfo();
p1.setName("Prabodh Kherade");
p1.setAddress("Chiplun");
p1.setAge(-40);
console.log("Name : " + p1.getName());
console.log("Age : " + p1.getAge());
console.log("Address : " + p1.getAddress());
