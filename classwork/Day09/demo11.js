var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
//Super class
var Person = /** @class */ (function () {
    //constructor
    function Person(name, address, age) {
        if (name === void 0) { name = ''; }
        if (address === void 0) { address = ''; }
        if (age === void 0) { age = 0; }
        console.log('Inside Person class ctor');
        this._name = name;
        this._address = address;
        this._age = age;
    }
    //facilitator
    Person.prototype.printInfo = function () {
        console.log("name : " + this._name);
        console.log("address : " + this._address);
        console.log("age : " + this._age);
    };
    return Person;
}());
//Sub class
var Employee = /** @class */ (function (_super) {
    __extends(Employee, _super);
    //constructor
    function Employee(id, name, address, age, company) {
        var _this = _super.call(this, name, address, age) || this;
        _this._id = id;
        _this._company = company;
        return _this;
    }
    //facilitator
    Employee.prototype.printInfo = function () {
        console.log("id : " + this._id);
        console.log("company : " + this._company);
        // console.log(`name : ${this._name}`)
        // console.log(`address : ${this._address}`)
        // console.log(`age : ${this._age}`)
        _super.prototype.printInfo.call(this);
    };
    return Employee;
}(Person));
var p1 = new Person('Jayesh Gopal', 'Pune', 23);
p1.printInfo();
//console.log(p1)
console.log('---------------------------------------');
var e1 = new Employee(1, 'Employee1', 'Pune', 25, 'company1');
//console.log(e1)
e1.printInfo();
