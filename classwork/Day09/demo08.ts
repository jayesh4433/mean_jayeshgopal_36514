class Person {
//Class members are Bydefault Public
//property
    private name:string
    private age:number
    private address:string

//Constructor
    public constructor(name:string = '',address:string = '',age:number = 0){
        console.log('Inside Constructor')
        this.name = name
        this.address = address
        this.age = age
    }

//getter
    public getName(){return this.name}
    public getAddress(){return this.address}
    public getAge(){return this.age}

//setter
    public setName(name: string){this.name = name}
    public setAddress(address: string){this.address = address}
    public setAge(age: number){
        if( (0< age) && (age<100) ){this.age = age}
        else{console.log('Age is not valid')}
    }

//facilitator
    public printInfo(){
        console.log(`Name : ${this.name}`)
        console.log(`Age : ${this.age}`)
        console.log(`Address : ${this.address}`)
    }
}

const p1 = new Person('Jayesh Gopal','Pune',23)
p1.printInfo()
p1.setName("Prabodh Kherade")
p1.setAddress("Chiplun")
p1.setAge(-40)
console.log(`Name : ${p1.getName()}`)
console.log(`Age : ${p1.getAge()}`)
console.log(`Address : ${p1.getAddress()}`)