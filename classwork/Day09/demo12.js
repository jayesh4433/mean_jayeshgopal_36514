var Rectangle = /** @class */ (function () {
    function Rectangle() {
    }
    Rectangle.prototype.erase = function () { console.log('rectangle is getting erased'); };
    Rectangle.prototype.draw = function () { console.log('rectangle is getting draw'); };
    return Rectangle;
}());
var Circle = /** @class */ (function () {
    function Circle() {
    }
    Circle.prototype.erase = function () { console.log('circle is getting erased'); };
    Circle.prototype.draw = function () { console.log('circle is getting draw'); };
    return Circle;
}());
var Square = /** @class */ (function () {
    function Square() {
    }
    Square.prototype.erase = function () { console.log('Square is getting erased'); };
    Square.prototype.draw = function () { console.log('Square is getting draw'); };
    return Square;
}());
function performOperation(shape) {
    shape.draw();
    shape.erase();
}
var r1 = new Rectangle();
performOperation(r1);
var c1 = new Circle();
performOperation(c1);
var s1 = new Square();
performOperation(s1);
