//create an object by using an object
const car = new Object()
car.model = 'i20-asta'
car.company = 'Hyundai'
car.price = 8.0

car .printInfo = function(){
    console.log(`model   = ${this.model}`)
    console.log(`company = ${this.company}`)
    console.log(`price   = ${this.price}`)
}
car.toString = function(){
    return `car [model   = ${this.model}, company = ${this.company}, price   = ${this.price}]`
}
// console.log(`car = ${car}, type = ${typeof(car)}`)
// car.printInfo()


//create object by using JSON
const person = {
    name: 'Jayesh Gopal',
    age: 23,
    email: 'jayesh@gmail.com',
    printInfo: function(){
        console.log(`name   =   ${this.name}`)
        console.log(`age    =   ${this.age}`)
        console.log(`email  =   ${this.email}`)
    },
    toString: function(){
        return `Person[name = ${this.name}, age = ${this.age}, email = ${this.email}]`
    }
}
// console.log(`person = ${person}, type = ${typeof(person)}`)
// person.printInfo()


//create object by using constructor fuction

function Mobile(model,price,company){
    this.model = model,
    this.company = company,
    this.price = price
}
Mobile.prototype.canAfford = function(){
    if(this.price < 30000)
    {
        console.log('can afford')
    }
    else
    {
        console.log('can not afford')
    }
}
Mobile.prototype.printInfo = function(){
    console.log(`Model = ${this.model}`)
    console.log(`Company = ${this.company}`)
    console.log(`Price = ${this.price}`)
}
Mobile.prototype.toString = function(){
    return `Mobile [Model = ${this.model}, Company = ${this.company}, Price = ${this.price}]`
}

const m1 = new Mobile("6T",28500,"One Plus")
m1.canAfford()
m1.printInfo()
console.log(`Mobile = ${m1} , type = ${typeof(m1)}`)