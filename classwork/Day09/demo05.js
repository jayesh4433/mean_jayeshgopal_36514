//variable
var age = 40;
//class
var Person = /** @class */ (function () {
    function Person() {
    }
    //method
    Person.prototype.canVote = function () {
        if (this.age >= 18) {
            console.log(this.name + " is eligible");
        }
        else {
            console.log(this.name + " is not eligible");
        }
    };
    Person.prototype.printInfo = function () {
        console.log("Name : " + this.name);
        console.log("Age : " + this.age);
        console.log("Address : " + this.address);
    };
    return Person;
}());
//creating object of class  Person
var p1 = new Person();
//setting up the properties
p1.name = 'Jayesh Gopal';
p1.age = 23;
p1.address = 'Pune';
//calling methods
// p1.canVote()
// p1.printInfo()
var Computer = /** @class */ (function () {
    function Computer() {
    }
    Computer.prototype.canAfford = function () {
        if (this.price <= 45000) {
            console.log('can afford');
        }
        else {
            console.log('can not afford');
        }
    };
    Computer.prototype.printInfo = function () {
        console.log("cpu : " + this.cpu);
        console.log("gpu : " + this.gpu);
        console.log("ram : " + this.ram);
        console.log("price : " + this.price);
    };
    return Computer;
}());
var c1 = new Computer();
c1.cpu = "intel i5";
c1.gpu = "NVDIA";
c1.ram = "8GB";
c1.price = 440000;
c1.canAfford();
c1.printInfo();
