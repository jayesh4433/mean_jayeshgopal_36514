//Explicit variable declaration [with data type]
var num = 10;
console.log("num = " + num + ", type = " + typeof (num));
var salary = 15.20;
console.log("salary = " + salary + ", type = " + typeof (salary));
var str = "Hello";
console.log("str = " + str + ", type = " + typeof (str));
var canVote = true;
console.log("canVote = " + canVote + ", type = " + typeof (canVote));
var myvar = undefined;
console.log("myvar = " + myvar + ", type = " + typeof (myvar));
//Generic Object
var person = { name: 'Jayesh Gopal', age: 23 };
console.log("person = " + person + ", type = " + typeof (person));
//Explicit Object
var mobile = { model: "One Plus 7", price: 33000 };
console.log("mobile = " + mobile + ", type = " + typeof (mobile));
//Union of string and number
var myvar2 = 'test';
myvar2 = 500;
//myvar2 = false //Not Ok
//Union of string & number  & boolean & object & undefined
var myvar3 = "Hello World";
myvar3 = 6450;
myvar3 = false;
myvar3 = { name: "Jayesh", age: 23 };
myvar3 = undefined;
//store any value
var myvar4 = "Hello World";
myvar4 = 6450;
myvar4 = false;
myvar4 = { name: "Jayesh", age: 23 };
myvar4 = undefined;
