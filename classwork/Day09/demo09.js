var Person = /** @class */ (function () {
    //Constructor
    function Person(name, address, age) {
        if (name === void 0) { name = ''; }
        if (address === void 0) { address = ''; }
        if (age === void 0) { age = 0; }
        console.log('Inside Constructor');
        this._name = name;
        this._address = address;
        this._age = age;
    }
    Object.defineProperty(Person.prototype, "name", {
        //getter
        get: function () { return this._name; },
        //setter
        set: function (name) { this._name = name; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Person.prototype, "address", {
        get: function () { return this._address; },
        set: function (address) { this._address = address; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Person.prototype, "age", {
        get: function () { return this._age; },
        set: function (age) {
            if ((0 < age) && (age < 100)) {
                this._age = age;
            }
            else {
                console.log('Age is not valid');
            }
        },
        enumerable: false,
        configurable: true
    });
    //facilitator
    Person.prototype.printInfo = function () {
        console.log("Name : " + this._name);
        console.log("Age : " + this._age);
        console.log("Address : " + this._address);
    };
    return Person;
}());
var p1 = new Person('Jayesh Gopal', 'Pune', 23);
p1.printInfo();
p1.name = "Prabodh Kherade";
p1.address = "Chiplun";
p1.age = -40;
console.log("Name : " + p1.name);
console.log("Age : " + p1.age);
console.log("Address : " + p1.address);
// p1.printInfo()
