class Person {
//Class members are Bydefault Public
//property
    private _name:string
    private _age:number
    private _address:string

//Constructor
    public constructor(name:string = '',address:string = '',age:number = 0){
        console.log('Inside Constructor')
        this._name = name
        this._address = address
        this._age = age
    }

//getter
    public get name(){return this._name}
    public get address(){return this._address}
    public get age(){return this._age}

//setter
    public set name(name:string){this._name = name}
    public set address(address: string){this._address = address}
    public set age(age: number){
        if( (0< age) && (age<100) ){this._age = age}
        else{console.log('Age is not valid')}
    }
//facilitator
    public printInfo(){
        console.log(`Name : ${this._name}`)
        console.log(`Age : ${this._age}`)
        console.log(`Address : ${this._address}`)
    }
}

const p1 = new Person('Jayesh Gopal','Pune',23)
p1.printInfo()
p1.name="Prabodh Kherade"
p1.address="Chiplun"
p1.age=-40
console.log(`Name : ${p1.name}`)
console.log(`Age : ${p1.age}`)
console.log(`Address : ${p1.address}`)
// p1.printInfo()