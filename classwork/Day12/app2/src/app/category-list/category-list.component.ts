import { CategoryService } from './../category.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-category-list',
  templateUrl: './category-list.component.html',
  styleUrls: ['./category-list.component.css']
})
export class CategoryListComponent implements OnInit {
  categories = []
  categoryService: CategoryService
  constructor(categoryService: CategoryService) {
    this.categoryService = categoryService
   }

  ngOnInit(): void {
    this.loadCategories()
  }
  loadCategories(){
    const  observable = this.categoryService.getCategory()
    observable.subscribe(response=>{
      if(response['status'] == 'success'){
        this.categories = response['data']
      }
    })
  }

}
