import { ProductService } from './../product.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {

  products = []
  productService: ProductService

  //Dependency Injection
  //create object of dependancy automatically
  //it uses singleton design pattern
  //product list component is depedent on product service
  constructor(productService: ProductService) {
    this.productService = productService
   }

  ngOnInit(): void {
    this.loadProducts()
  }

  loadProducts(){
    //component object got created successfully
   // this.products = this.productService.getProducts()
   const observable = this.productService.getProducts()
   observable.subscribe(response=>{
     if(response['status']=='success'){
       this.products = response['data']
     } 
   })
  }

  onEdit(product){

  }

  onDelete(product){
    const observable = this.productService.deleteProduct(product.id)
   observable.subscribe(response=>{
     if(response['status']=='success'){
       alert(`${product.title} is deleted`)
     }
     this.loadProducts() 
   })
  }

}
