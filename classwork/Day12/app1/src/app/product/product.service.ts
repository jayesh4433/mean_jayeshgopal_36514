import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
@Injectable({
  providedIn: 'root'
})
export class ProductService {
  private url = 'http://localhost:4433/product'
  private httpClient: HttpClient
  constructor(httpClient: HttpClient) {
    this.httpClient = httpClient
   }

  getProducts(){
    //send get request to middleware
    // const observable = this.httpClient.get(this.url)

    //when response is received, consume it
    // observable.subscribe(response =>{
    //   console.log(response)
    // })

    return this.httpClient.get(this.url)
  }
  
  deleteProduct(id){
    return this.httpClient.delete(this.url+'/'+id)
  }

  addProduct(title:string,description:string,price:number,category:number){
    const body = {
      title: title,
      description: description,
      price: price,
      category: category
    }
    return this.httpClient.post(this.url,body)
  }
}
