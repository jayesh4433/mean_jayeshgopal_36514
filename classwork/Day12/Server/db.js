const mysql = require('mysql2')

const pool = mysql.createPool({
    user: 'mean',
    password: 'mean',
    host: 'localhost',
    database: 'mystore',
    waitForConnections: true,
    connectionLimit: 10,
    queueLimit:0
})

module.exports = pool