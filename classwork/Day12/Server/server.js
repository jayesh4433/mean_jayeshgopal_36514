const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')

//router
const userRouter = require('./routes/user')
const productRouter = require('./routes/product')
const categoryRouter = require('./routes/category')

//
const app = express()


app.use(bodyParser.json())
app.use(cors('*'))
//routes
app.use('/user',userRouter)
app.use('/product',productRouter)
app.use('/category',categoryRouter)


//start listening
app.listen(4433,'0.0.0.0',()=>{
    console.log('server started on 4433')
})