const express = require('express')
const db = require('../db')
const utils = require('../utils')

const router = express.Router()

//create
router.post('/',(request,response)=>{
    const{title,description,price,category} = request.body

    const statement = 'insert into product(title,description,price,category_id) '+
    `values('${title}','${description}',${price},${category})`

    db.query(statement,(error,data)=>{
        response.send(utils.createResult(error,data))
    })   
})

//read
router.get('/',(request,response)=>{

    const statement = 'select * from product'

    db.query(statement,(error,data)=>{
        response.send(utils.createResult(error,data))
    })   
})

//update
router.put('/:id',(request,response)=>{
    const{ id } = request.params
    const{title,description,price,category} = request.body

    const statement = `update product set title='${title}',description='${description}',`+
    `price=${price} ,category_id = ${category} where id=${id}`

    db.query(statement,(error,data)=>{
        response.send(utils.createResult(error,data))
    })   
})

//delete
router.delete('/:id',(request,response)=>{
    const{ id } = request.params

    const statement = `delete from product where id = ${id}`

    db.query(statement,(error,data)=>{
        response.send(utils.createResult(error,data))
    })   
})


module.exports = router