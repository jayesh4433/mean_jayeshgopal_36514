const express = require('express')
const db = require('../db')
const utils = require('../utils')

const router = express.Router()

//create
router.post('/',(request,response)=>{
    const{title,description} = request.body

    const statement = 'insert into category(title,description) '+
    `values('${title}','${description}')`

    db.query(statement,(error,data)=>{
        response.send(utils.createResult(error,data))
    })   
})

//read
router.get('/',(request,response)=>{

    const statement = 'select * from category'

    db.query(statement,(error,data)=>{
        response.send(utils.createResult(error,data))
    })   
})

//update
router.put('/:id',(request,response)=>{
    const{ id } = request.params
    const{title,description} = request.body

    const statement = `update category set title='${title}',description='${description}' where id=${id}`

    db.query(statement,(error,data)=>{
        response.send(utils.createResult(error,data))
    })   
})

//delete
router.delete('/:id',(request,response)=>{
    const{ id } = request.params

    const statement = `delete from category where id = ${id}`

    db.query(statement,(error,data)=>{
        response.send(utils.createResult(error,data))
    })   
})


module.exports = router