const express = require('express')
const crypto = require('crypto-js')
const db = require('../db')
const utils = require('../utils')

const router = express.Router()

//Sign In / Log In
router.post('/signin',(request,response)=>{
    const {email,password} = request.body
    const encryptPassword = crypto.SHA256(password)
    const statement = `select id,firstName,lastName from user where email='${email}' and password='${encryptPassword}'`

    db.query(statement,(error,users)=>{
        const result = {}
        if(error){
            result['status'] = 'error'
            result['error'] = error
        }
        else{
            if(users.length == 0){
                result['status'] = 'error'
                result['error'] = 'Incorrect UserName / Password'
            }
            else{
                result['status'] = 'success'
                result['data'] = users[0]
            }
        }
        response.send(result)
    })
})


//Sign Up
router.post('/signup',(request,response)=>{
    const {firstName,lastName,email,password,address,phone} = request.body
    const encryptPassword = crypto.SHA256(password)
    const statement = 'insert into user(firstName,lastName,email,password,address,phone) '+
    `values('${firstName}','${lastName}','${email}','${encryptPassword}','${address}','${phone}')`

    db.query(statement,(error,data)=>{
        response.send(utils.createResult(error,data))
    })
})

module.exports = router