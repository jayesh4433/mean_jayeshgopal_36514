const http = require('http')

//To create server
const server = http.createServer((request,response)=>{
    console.log('Request is received from client')

    //send response to client
    /* response.setHeader('content-Type','application/json')
    response.end('{"name":"Jayesh","Address":"Pune","Age":23}')
 */
    response.setHeader('content','text/html')
    response.end("<h2>Welcome</h2><p>................</p>")
    
})


//start the server listening
server.listen(4433,'localhost',()=>{
    console.log(`Server started successfully on port 4433`)
})