const fs = require('fs')

//reading file in sync
/* function readSync() 
{

    console.log('reading file started')
    const data = fs.readFileSync('./welcome.txt')// blocking API
    console.log('Reading file completed')

    console.log(`Data = ${data}`)


    console.log('started another task')
    const result = 123456789*987456321
    console.log('Task completed')
    console.log(`Result = ${result}`)

} */
function readSync() 
{

    try 
    {
        console.log('reading file started')
        const data = fs.readFileSync('./welcome.txt')// blocking API
        console.log('Reading file completed')

        console.log(`Data = ${data}`)
    } 
    catch (error) 
    {
        console.log(`Error = ${error}`)
    }
    


    console.log('started another task')
    const result = 123456789*987456321
    console.log('Task completed')
    console.log(`Result = ${result}`)

}
/* readSync() */

function readAsync() 
{
    fs.readFile('./welcome.txt',(error,data)=>{
        console.log('Inside Arrow Function')
        if(error)
        {
            console.log(`Error = ${error}`)
        }
        else
        {
            console.log('Reading file completed')
            console.log(`Data = ${data}`)
        }
    })

    console.log('started another task')
    const result = 123456789*987456321
    console.log('Task completed')
    console.log(`Result = ${result}`)
    
}

readAsync()