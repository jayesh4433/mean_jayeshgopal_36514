const http = require('http')

//To create server
const server = http.createServer((request,response)=>{
    console.log('Request is received from client')

    console.log(`url    :   ${request.url}`)
    console.log(`method :   ${request.method}`)

    if(request.url == '/dept')
    {
        if(request.method == 'GET'){console.log('select * from dept')}
        else if(request.method == 'POST'){console.log("insert into dept(deptName,location) values('HR','Pune')")}
        else if(request.method == 'PUT'){console.log("update dept deptName='Prod' location='Mumbai' where deptNo=3")}
        else if(request.method == 'DELETE'){console.log("delete from dept where deptNo=2")}

    }
    else if(request.url == '/emp')
    {
        if(request.method == 'GET'){console.log('select * from emp')}
        else if(request.method == 'POST'){console.log("insert into emp(name,address) values('Jayesh','Pune')")}
        else if(request.method == 'PUT'){console.log("update dept name='Prabodh' location='Chiplun' where empNo=3")}
        else if(request.method == 'DELETE'){console.log("delete from emp where empNo=2")}

    }
    console.log('------------------------------------------------')
    //send response to client
    response.end('.')
    
})


//start the server listening
server.listen(4433,'localhost',()=>{
    console.log(`Server started successfully on port 4433`)
})