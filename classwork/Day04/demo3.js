const http = require('http')

//To create server
const server = http.createServer((request,response)=>{
    console.log('Request is received from client')

    //send response to client
    response.end(" Welcome :) ")
})


//start the server listening
server.listen(4433,'localhost',()=>{
    console.log(`Server started successfully on port 4433`)
})