create database evernote;
use evernote;
create table user (
    id integer primary key auto_increment, 
    name varchar(100), 
    email varchar(100), 
    password varchar(100), 
    phone varchar(20), 
    address varchar(100),
    activationToken varchar(100),
    active int(1) default 0
    );

create table notes (
    id integer primary key auto_increment, 
    title varchar(100), 
    contents varchar(1024), 
    userId integer, 
    isPrivate int(1) default 1, 
    file varchar(100), 
    createdOn TIMESTAMP DEFAULT CURRENT_TIMESTAMP
    );