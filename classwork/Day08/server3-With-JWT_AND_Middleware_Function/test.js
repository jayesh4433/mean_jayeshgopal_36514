const cryptojs = require('crypto-js')
const secret = "5492akgaulbjrrg35d1w3a20dwdg+9626+whd24"

/*===================================================================================================*/
//authenticated user
const data = {userId : 1}
const userData = {data:data,hash:`${cryptojs.SHA256(JSON.stringify(data)+secret)}`}
const strUserData = JSON.stringify(userData)
console.log(`user data = ${strUserData}`)
//authenticated user
/*===================================================================================================*/



/*===================================================================================================*/
//hacker user
const strHackerData = '{"data":{"userId":1},"hash":"27913ceffd66c9b63fbe457eafb0b64e937eff7ea067f50e12923baab265a9f0"}'
const hackerData = JSON.parse(strHackerData)
hackerData['data']['userId'] = 2
hackerData['hash'] = `${cryptojs.SHA256(JSON.stringify(hackerData['data']))}`

const modifiedData = JSON.stringify(hackerData)
console.log(`hacker has modified the request : ${modifiedData}`)

//hacker user
/*===================================================================================================*/



/*===================================================================================================*/
//server
//const strServerData = '{"data":{"userId":1},"hash":"27913ceffd66c9b63fbe457eafb0b64e937eff7ea067f50e12923baab265a9f0"}'
const strServerData = '{"data":{"userId":2},"hash":"d447c5b7cab10e176c8f4f5b5efd95d23295581d071181ccd37dece62f98ff9a"}'
console.log(`Server Received = ${strServerData}`)

const serverData = JSON.parse(strServerData)
const serverHash = cryptojs.SHA256(JSON.stringify(serverData['data']) + secret)
console.log(`ServerHash : ${serverHash}`)
console.log(`ClientHash : ${serverData['hash']}`)
if(serverHash == serverData['hash'])
{
    console.log(`delete from notes where userId = ${serverData['data']['userId']}`)
}
else
{
    console.log('The request has been modified in between')
}
//server
/*===================================================================================================*/