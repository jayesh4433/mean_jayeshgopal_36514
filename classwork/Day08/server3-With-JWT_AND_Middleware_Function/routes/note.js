const express = require('express')
const db = require('../db')
const util = require('../util')
//const jwt = require('jsonwebtoken')
const config = require('../config')

const router = express.Router()

router.post('/',(request,response)=>{
    const {title,contents} = request.body

    const statement = 'INSERT INTO notes(title,contents,userId) '+
    `VALUES ('${title}','${contents}',${request.userId})`

    db.connection.query(statement,(error,data)=>{
        response.send(util.creatResult(error,data))
    })

})
 
router.get('/',(request,response)=>{

    const statement = `SELECT * FROM notes WHERE userId=${request.userId}`

    db.connection.query(statement,(error,data)=>{
        response.send(util.creatResult(error,data))
    })

})

router.put('/:noteId',(request,response)=>{
    const {noteId} = request.params
    const {title,contents} = request.body
    
    const statement = `UPDATE notes SET title='${title}',
    contents='${contents}' WHERE id= ${noteId}`

    db.connection.query(statement,(error,data)=>{
        response.send(util.creatResult(error,data))
    })
    
})

router.delete('/single/:noteId',(request,response)=>{
    const {noteId} = request.params
   
    const statement = `DELETE FROM notes WHERE id=${noteId}`

    db.connection.query(statement,(error,data)=>{
        response.send(util.creatResult(error,data))
    })
   
})

router.delete('/all/',(request,response)=>{

    const statement = `DELETE FROM notes WHERE userId=${request.userId}`

    db.connection.query(statement,(error,data)=>{
        response.send(util.creatResult(error,data))
    })

})

module.exports = router