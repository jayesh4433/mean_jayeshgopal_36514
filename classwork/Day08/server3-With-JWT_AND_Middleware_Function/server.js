const express = require('express')
const bodyParser = require('body-parser')
const jwt = require('jsonwebtoken')

const config = require('./config')
const util = require('./util')

//Get Routes
const userRouter = require('./routes/user')
const noteRouter = require('./routes/note')

const app = express()

function authorizeUser(request,response,next)
{
    if ( (request.url == '/user/signin') || (request.url == '/user/signup') ||
         (request.url.startsWith('/user/activate/'))
        ) 
        {
            next()
        } 
        else 
        {
            const token = request.headers['token']
            if (!token) 
            {
                response.status(401)
                response.send(util.creatResult('Token is missing'))
            } 
            else 
            {
                try 
                {
                    const data = jwt.verify(token,config.secret)
                    request.userId = data.id
                    next()

                } 
                catch(ex) 
                {
                    response.status(401)
                    response.send(util.creatResult('Invalid token'))
                }
            }
        }

}

app.use(bodyParser.json())
app.use(authorizeUser)
//Add Routes
app.use('/user',userRouter)
app.use('/note',noteRouter)


app.listen(4433,'localhost',()=>{
    console.log('Server is started on port 4433')
})