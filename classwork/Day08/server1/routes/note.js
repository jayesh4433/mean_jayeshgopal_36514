const express = require('express')
const db = require('../db')
const util = require('../util')
const { request, response } = require('express')


const router = express.Router()

router.post('/:userId',(request,response)=>{
    const {userId} = request.params
    const {title,contents} = request.body

    const statement = 'INSERT INTO notes(title,contents,userId) '+
    `VALUES ('${title}','${contents}',${userId})`

    db.connection.query(statement,(error,data)=>{
        response.send(util.creatResult(error,data))
    })
})
 
router.get('/:userId',(request,response)=>{
    const {userId} = request.params

    const statement = `SELECT * FROM notes WHERE userId=${userId}`

    db.connection.query(statement,(error,data)=>{
        response.send(util.creatResult(error,data))
    })
})

router.put('/:noteId',(request,response)=>{
    const {noteId} = request.params
    const {title,contents} = request.body

    const statement = `UPDATE notes SET title='${title}',
    contents='${contents}' WHERE id= ${noteId}`

    db.connection.query(statement,(error,data)=>{
        response.send(util.creatResult(error,data))
    })
})

router.delete('/single/:noteId',(request,response)=>{
    const {noteId} = request.params

    const statement = `DELETE FROM notes WHERE id=${noteId}`

    db.connection.query(statement,(error,data)=>{
        response.send(util.creatResult(error,data))
    })
})

router.delete('/all/:userId',(request,response)=>{
    const {userId} = request.params

    const statement = `DELETE FROM notes WHERE userId=${userId}`

    db.connection.query(statement,(error,data)=>{
        response.send(util.creatResult(error,data))
    })
})

module.exports = router