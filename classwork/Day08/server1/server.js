const express = require('express')
const bodyParser = require('body-parser')

//Get Routes
const userRouter = require('./routes/user')
const noteRouter = require('./routes/note')

const app = express()

app.use(bodyParser.json())
//Add Routes
app.use('/user',userRouter)
app.use('/note',noteRouter)


app.listen(4433,'localhost',()=>{
    console.log('Server is started on port 4433')
})