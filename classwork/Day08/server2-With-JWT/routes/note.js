const express = require('express')
const db = require('../db')
const util = require('../util')
const jwt = require('jsonwebtoken')

const router = express.Router()

router.post('/',(request,response)=>{
    const token = request.headers['token']
    const {title,contents} = request.body

    try
    {
        const data = jwt.verify(token,'r98451da51ht5a1f54h15z16sr54hs5wr5h4rb4wb1wc6w')
        //console.log(data.id)
        const statement = 'INSERT INTO notes(title,contents,userId) '+
        `VALUES ('${title}','${contents}',${data.id})`

        db.connection.query(statement,(error,data)=>{
            response.send(util.creatResult(error,data))
        })
    }
    catch(ex){
        response.status(401)
        response.send(util.creatResult('Invalid token'))
    }
})
 
router.get('/',(request,response)=>{
    const token = request.headers['token']
    try
    {
        const data = jwt.verify(token,'r98451da51ht5a1f54h15z16sr54hs5wr5h4rb4wb1wc6w')
        //console.log(data.id)
        const statement = `SELECT * FROM notes WHERE userId=${data.id}`

        db.connection.query(statement,(error,data)=>{
            response.send(util.creatResult(error,data))
        })
    }
    catch(ex){
        response.status(401)
        response.send(util.creatResult('Invalid token'))
    }
})

router.put('/:noteId',(request,response)=>{
    const {noteId} = request.params
    const {title,contents} = request.body
    
    const statement = `UPDATE notes SET title='${title}',
    contents='${contents}' WHERE id= ${noteId}`

    db.connection.query(statement,(error,data)=>{
        response.send(util.creatResult(error,data))
    })
    
})

router.delete('/single/:noteId',(request,response)=>{
    const {noteId} = request.params
   
    const statement = `DELETE FROM notes WHERE id=${noteId}`

    db.connection.query(statement,(error,data)=>{
        response.send(util.creatResult(error,data))
    })
   
})

router.delete('/all/',(request,response)=>{
    const token = request.headers['token']
    try
    {
        const data = jwt.verify(token,'r98451da51ht5a1f54h15z16sr54hs5wr5h4rb4wb1wc6w')
        const statement = `DELETE FROM notes WHERE userId=${data.id}`

        db.connection.query(statement,(error,data)=>{
            response.send(util.creatResult(error,data))
        })
    }
    catch(ex){
        response.status(401)
        response.send(util.creatResult('Invalid token'))
    }
})

module.exports = router