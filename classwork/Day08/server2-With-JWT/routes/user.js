const express = require('express')
const db = require('../db')
const util = require('../util')
const crypto = require('crypto-js')
const uuid = require('uuid')
const mailer = require('../mailer')
const jwt = require('jsonwebtoken')

const router = express.Router()

//SignUp
router.post('/signup',(request,response)=>{
    const {name,email,password,phone,address} = request.body
    const encryptPassowrd = crypto.SHA256(password)

    //Create UUID for user activation
    const activationToken = uuid.v4()

    const statement = 'INSERT INTO user (name,email,password,phone,address,activationToken) '+
    `VALUES ('${name}','${email}','${encryptPassowrd}','${phone}','${address}','${activationToken}')`

    db.connection.query(statement,(error,user)=>{
        if(!error)
        {
            const body = `
            <h1>welcome to the evernote application</h1>
            <div>
                    please activate your account here 
                    <div>
                    <a href="http://localhost:4433/user/activate/${activationToken}">activate my account</a>
                    </div>
            </div>`
            mailer.sendEmail(email,'Activate your account',body,(mailError,mailResult)=>{
                response.send(util.creatResult(error,user))
            })
        }
        else
        {
            response.send(util.creatResult(error,user)) 
        }
    })
})

//Signin
router.post('/signin',(request,response)=>{
    const {email,password} = request.body
    const encryptPassowrd = crypto.SHA256(password)

    const statement = 'SELECT id,name,phone,address,active FROM user WHERE '+
    `email = '${email}' AND password = '${encryptPassowrd}'`

    db.connection.query(statement,(error,users)=>{
        const result = {}

        if(error){result['status']='error';result['error']=error}
        else{
            if(users.length == 0){
                result['status']='error';
                result['error'] = 'Incorrect User Name/Password'
            }
            else
            {
                const user = users[0]
                if(user['active'] == 0)
                {
                    result['status']='error';
                    result['error'] = 'your account is not active.'+
                    'please activate your account by using the link you have'+
                    ' received in your registration confirmation email.'
                }
                else if(user['active'] == 1)
                {
                    const authToken = jwt.sign({id: user['id']},'r98451da51ht5a1f54h15z16sr54hs5wr5h4rb4wb1wc6w')

                    result['status'] = 'success'
                    result['data'] = {
                        name:user['name'],
                        address:user['address'],
                        phone:user['phone'],
                        authToken:authToken
                        
                    }
                }
            }
        }
        response.send(result)
    })
})

router.get('/activate/:token',(request,response)=>{

    const {token} = request.params

    const statement = `update user set active=1 where activationToken='${token}'`

    db.connection.query(statement,(error,data)=>{
        let body = ''
        if (error) {
            body = `
              <h1>Error occurred whiel activating your account..</h1>
              <h5>${error}</h5>
            `
          } else {
            body = `
              <h1>Congratulations!!! Successfully activated your account.</h1>
              <h5>Please login to continue</h5>
            `
          }
           //TODO - Deletation of token
        response.send(body)
    })
})

router.get('/profile',(request,response)=>{
    const token = request.headers['token']

    try
    {
        //verify token
        const data = jwt.verify(token,'r98451da51ht5a1f54h15z16sr54hs5wr5h4rb4wb1wc6w')

        const statement = `SELECT id,name,phone,address FROM user WHERE id=${data.id}`

        db.connection.query(statement,(error,data)=>{
            response.send(util.creatResult(error,data))
        })
    }
    catch(ex){
        response.status(401)
        response.send(util.creatResult('Invalid token'))
    }
})
module.exports = router