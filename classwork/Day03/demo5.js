function person(name,address,age) {

    this.name = name
    this.address = address
    this.age = age
}

//below method will share among all person object
person.prototype.printInfo = function () {
    console.log(`Name   :   ${this.name}`)
    console.log(`Address:   ${this.address}`)
    console.log(`Age    :   ${this.age}`)
    console.log('-----------------------------')
}

//below method will share among all person object
person.prototype.canVote = function() {
    if(this.age >= 18)
    {
        console.log(`${this.name} is eligible for voting`)
    }
    else
    {
        console.log(`${this.name} is not eligible for voting`)
    }
    
}
const p1 = new person("Prabodh","Chiplun",23)
const p2 = new person("Jayesh","Pune",17)

p1.printInfo()
p1.canVote()
console.log()
console.log()
p2.printInfo()
p2.canVote()