function person(name,address,age) {

    this.name = name
    this.address = address
    this.age = age
}
//below method will share among all person object
person.prototype.printInfo = function () {
    console.log(`Name   :   ${this.name}`)
    console.log(`Address:   ${this.address}`)
    console.log(`Age    :   ${this.age}`)
    console.log('-----------------------------')
}


const p1 = new person("Prabodh","Chiplun",23)
const p2 = new person("Jayesh","Pune",23)

p1.printInfo()
p2.printInfo()