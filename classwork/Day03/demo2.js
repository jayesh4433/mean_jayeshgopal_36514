const person = new Object()

person.name = "Jayesh"
person.address = "Pune"
person.age = 23

person.printInfo = function() {
    console.log(`name   :   ${this.name}`)
    console.log(`address:   ${this.address}`)
    console.log(`age    :   ${this.age}`)    
}

person.printInfo()