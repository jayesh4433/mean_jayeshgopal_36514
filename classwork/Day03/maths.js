function add(num1,num2) {
    console.log(`Result :   ${num1+num2}`)
}

function sub(num1,num2) {
    console.log(`Result :   ${num1-num2}`)
}

function mul(num1,num2) {
    console.log(`Result :   ${num1*num2}`)
}

function div(num1,num2) {
    console.log(`Result :   ${num1/num2}`)
}

module.exports = {
    'add' : add,
    'sub' : sub,
    'mul' : mul,
    'div' : div
}