const person = {
    name: "Jayesh",
    address: "Pune",
    age: 23,

    printInfo: function(){
        console.log(`name   :   ${this.name}`)
        console.log(`address:   ${this.address}`)
        console.log(`age    :   ${this.age}`)
    } 

}
person.printInfo()