const os = require('os')

console.log('--- cpu info ---')
const cpus = os.cpus()
for (const cpu of cpus) {
  console.log(`model: ${cpu.model}, speed: ${cpu.speed}`)
}
console.log('--- cpu info ---')
console.log('')

console.log('--- network interfaces ---')

const networkInterfaces = os.networkInterfaces()
console.log(networkInterfaces)


console.log('--- network interfaces ---')
console.log('')


console.log(`platform: ${os.platform()}`)
console.log(`OS release: ${os.release()}`)
console.log(`architecture: ${os.arch()}`)
console.log(`memory: ${os.freemem() / (1024 * 1024 * 1024)} GB`)
console.log(`memory: ${os.totalmem() / (1024 * 1024 * 1024)} GB`)

console.log(`home directory: ${os.homedir()}`)
console.log(`hostname: ${os.hostname()}`)
