function person(name,address,age) {

    this.name = name
    this.address = address
    this.age = age
}

//toString Overriding
person.prototype.toString = function() {
    return `Name :  ${this.name}, Address:    ${this.address}, Age :   ${this.age}`
}

const p1 = new person("Jayesh","Pune",23)
console.log(`p1 = ${p1}`) //this will internally call toString()

const p2 = new person("Prabodh","Chiplun",23)
console.log(`p2 = ${p2}`)//this will internally call toString()

const p3 = new person("Akash","Indoli",23)
p3.toString = function() {
    return `Name :  ${this.name}, Address:   ${this.address}`
}
console.log(`p3 = ${p3}`)