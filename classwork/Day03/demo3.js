function person(name,address,age) {

    this.name = name
    this.address = address
    this.age = age
}

const p1 = new person('Jayesh','Pune',23)
p1.printInfo = function() {
    console.log(`Name   :   ${this.name}`)
    console.log(`Address:   ${this.address}`)
    console.log(`Age    :   ${this.age}`)
}

const p2 = new person('Prabodh','Chiplun',23)
p2.printInfo = function() {
    console.log(`Name   :   ${this.name}`)
    console.log(`Address:   ${this.address}`)
    console.log(`Age    :   ${this.age}`)
}

p1.printInfo()
p2.printInfo()

console.log(p1)
console.log(p2)